<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'github' => [
        'client_id' => 'c676ad6277e7e72a0f8e',
        'client_secret' => '437fe0d5d45295912188444ef820f3ecdd592efc',
        'redirect' => 'https://uunzi.com/login/github/callback',
    ],
    'twitter' => [
        'client_id' => '7L7UsIaw2BSyLaY2cwLQmmML8',
        'client_secret' => 'JpFFZ8WRcOzu19edQa3RUYD5jTJCqQzmw9ENHGqQuTtTU1pLPG',
        'redirect' => 'https://uunzi.com/login/twitter/callback'
    ],
    'facebook' => [
        'client_id' => '1011795735840508',
        'client_secret' => '2e51858ab9c07fe470c7968a3a0248be',
        'redirect' => 'https://uunzi.com/login/facebook/callback'
    ],
    'google' => [
        'client_id' => '1022063865441-nrcu6smpbo83p2dtom3qt2l7ssukj58i.apps.googleusercontent.com',
        'client_secret' => 'NlNJ4g7wD-BAnuX3f27yIpo_',
        'redirect' => 'https://uunzi.com/login/google/callback'
    ]


];
