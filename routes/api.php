<?php

use Illuminate\Http\Request;
use App\Innovator;
use App\Post;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); **/

//get all innovators
Route::get('/innovators', 'Api\ApiController@getInnovators');

//Get info about a specific innovator
Route::get('/innovators/{innovator}', 'Api\ApiController@getInnovator');

//get all the posts
Route::get('/posts', 'Api\ApiController@getPosts');

//get a particular post
Route::get('/posts/{post}', 'Api\ApiController@getPost');

Route::post('validate/email', 'Api\ApiController@validateEmail');
