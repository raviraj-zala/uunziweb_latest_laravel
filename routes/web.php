<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
    | Here is where you can   web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'LandingPage\LandingPageController@index');

Route::get('search/', 'Search\SearchController@handle');
Route::post('filter/search/', 'Search\SearchController@filter');
Route::get('find/', 'Search\SearchController@search')->middleware('api');
Route::get('get-user-review/{innovator}', 'Api\ApiController@getUserReview');
Route::resource('innovator-reviews', 'InnovatorReviewController');


// newsletter
Route::post('subscribe-to-newsletter', 'NewsLetter\NewsLetterController@handle');

// get users
Route::resource('users', 'UserController')->middleware(['auth', 'admin']);

// Route::view('add-your-company', 'public.addcompany')->name('add.company');
Route::view('loan-calculator', 'loan')->name('loan.calculator');
Route::view('/faqs', 'howitworks');
Route::get('uunzi-community', 'Community\CommunityController@index');
Route::post('add-your-company', 'Listing\GuestController@handle');
Route::get('get-simmilar-innovators/{innovator}', 'InnovatorController@getSimmilar');

Route::view('about-us', 'about');
Route::view('privacy-policy', 'privacy-policy');
Route::resource('events', 'EventController');
Route::resource('innovators', 'InnovatorController')->only('show');
Route::resource('innovators', 'InnovatorController')->middleware(['auth', 'admin'])->except('show');

Route::resource('posts', 'PostController')->middleware(['auth', 'admin'])->except('show');
Route::resource('tags', 'TagController')->middleware(['auth', 'admin'])->except('show');



Route::resource('innovator-categories', 'InnovatorCategoryController')->only('show');

Route::resource('innovator-categories', 'InnovatorCategoryController')->middleware(['auth', 'admin'])->except('show');



Route::resource('innovator-subcategories', 'InnovatorSubcategoryController');
Route::get('innovator-get-subcategories', 'InnovatorSubcategoryController@indexget');
Route::get('innovator-subcategories-min-max-index', 'InnovatorSubcategoryController@showIndex_Min_Max_Loan_Amount');
Route::get('innovator-subcategories-review-index', 'InnovatorSubcategoryController@showIndex_Review');
Route::get('innovator-subcategories-intrest-index', 'InnovatorSubcategoryController@showIndex_Interest');
Route::get('innovator-subcategories-repayment-index', 'InnovatorSubcategoryController@showIndex_Repaymnet');
Route::post('innovator-subcategories-data', 'InnovatorSubcategoryController@indexdata');
Route::get('showIndex_Repaymnet', 'InnovatorSubcategoryController@testReview');
Route::get('test-review-new', 'InnovatorSubcategoryController@testReview');

Route::resource('category', 'InnovatorSubcategoryController');
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');


// Third Party login
Route::get('login/github', 'SocialLogin@githubLogin');
Route::get('login/github/callback', 'SocialLogin@githubCallback');
Route::get('login/twitter', 'SocialLogin@twitterLogin');
Route::get('login/twitter/callback', 'SocialLogin@twitterCallback');
Route::get('login/facebook', 'SocialLogin@facebookLogin');
Route::get('login/facebook/callback', 'SocialLogin@facebookCallback');
Route::get('login/google', 'SocialLogin@googleLogin');
Route::get('login/google/callback', 'SocialLogin@googleCallback');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('innovator-form-review', 'InnovatorReviewController@addreviewpage')->name('review_form');
    Route::get('reviews-app', 'InnovatorReviewController@getreviewapps');
    Route::post('innovator-add-review', 'InnovatorReviewController@addreview');

});
//admin group
Route::prefix('uunzi-admin')->group(function () {
    Route::middleware(['auth', 'admin'])->group(function () {
        Route::post('update-innovator-logo/{innovator}', 'Admin\Dashboard@updateInnovatorLogo');
        Route::get('/', 'Admin\Dashboard@index')->name('admin');
        Route::get('users', 'Admin\Dashboard@users')->name('admin.users');
        Route::get('posts', 'Admin\Dashboard@posts')->name('admin.posts');
        Route::get('innovators', 'Admin\Dashboard@innovators')->name('admin.innovators');
        Route::get('innovators-categories', 'Admin\Dashboard@categories')->name('admin.categories');
        Route::get('get-data', 'Admin\Dashboard@getData')->name('admin.get.data');
        Route::get('get-innovators', 'Admin\Dashboard@getInnovators');
        Route::get('get-categories', 'Admin\Dashboard@getCategories');
        Route::get('get-subcategories', 'Admin\Dashboard@getSubCategories');
        Route::get('innovator-verify/{innovator}', 'Admin\Dashboard@handleInnovatorVerification');
        Route::get('get-trashed-innovators', 'Admin\Dashboard@getTrashedInnovators');
        Route::delete('innovator-delete/{innovator}', 'Admin\Dashboard@forceDeleteInnovator');
        Route::delete('category-delete/{category}', 'Admin\Dashboard@forceDeleteCategory');
        Route::get('innovator-restore/{innovator}', 'Admin\Dashboard@restoreInnovator');
        Route::post('contact-user', 'Admin\Dashboard@contactUser');


    });
});

// prevent normal users from viewing ckfinder browser

Route::get('ckfinder/browser', 'LandingPage\LandingPageController@ckFinder');

//show post has to be the last route
Route::get('/{post}', 'PostController@show');
