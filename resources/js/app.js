/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');
require('./cookies')
import '@babel/polyfill'
import Vue from 'vue'

import vuetify from './vuetify'
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/dist/vuetify.css'
import CKEditor from '@ckeditor/ckeditor5-vue'
window.ClassicEditor = require("@ckeditor/ckeditor5-build-classic");
import SocialSharing from 'vue-social-sharing'

Vue.use(SocialSharing)

Vue.use(CKEditor)

import VeeValidate from 'vee-validate';
import { Validator } from 'vee-validate'
Vue.use(VeeValidate)

const isUnique = (value) => {
    return axios.post('/api/validate/email', { email: value }).then((response) => {
        // Notice that we return an object containing both a valid property and a data property.
        return {
            valid: response.data.valid,
            data: {
                message: response.data.message
            }
        };
    });
};


Validator.extend('unique', {
    validate: isUnique,
    getMessage: (field, params, data) => {
        return data.message;
    }
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */


const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


const app = new Vue({
    vuetify,
    el: "#app",
    data: function () {
        return {
            drawer: false,
            admin_drawer: null,
            auth_dialog: false
        }
    },
    methods: {

        logout() {
            axios.post('/logout')
                .then(res => {
                    location.reload(true)
                })
        },profile() {
            axios.post('/')
                .then(res => {
                    location.reload(true)
                })
        },
        search() {
            document.getElementById('searchForm').submit()
        }
    }, mounted() {
        if (cookie('has_login') !== undefined) {
            setTimeout(() => {
                this.auth_dialog = true
            }, 5000);
            setInterval(() => {
                this.auth_dialog = true
            }, 50000);
        }
    }
});