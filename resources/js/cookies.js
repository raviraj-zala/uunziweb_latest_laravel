/**
 * All the actions that need cookies
 * 1. Show pop ups to prompt users
 * 
 * cookie name for has not logged in
 * has_login
 * 
 * Cookie name for newsletter
 * newsletter
 */

cookie.set('has_login', 0, { path: '/', expires: 1 / 24 / 60 })

cookie.set('newsletter', 0, { path: '/', expires: 1 / 24 / 60 })

