@extends('layouts.app')
@section('title', $results->name)
@section('description', $results->name)
@section('content')
    <section data-banner-section="">
        <v-card flat tile>
            <v-img src="/images/uunzi-main-image-blue.png" max-height="350px">
                <v-layout column align-center class="white--text">
                    <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>
                    <h1 class="white--text mb-2 headingLora text-center">{{ $results->name }}</h1>
                    <bread-crumbs :itemms="[
                            {
                                text: 'Uunzi',
                                disabled: false,
                                href: '/',
                            },
                            {
                                text: 'Categories',
                                disabled: true,
                                href: '/category',
                            },
                            {
                                text: '{{ $results->name }}',
                                disabled: true,
                                href: '/categories/{{ $results->name }}'
                            }]">
                    </bread-crumbs>
                </v-layout>
            </v-img>
        </v-card>
    </section>
    <section data-innovators-section="">
        <h1 style="color:#ff8236;" class="mb-2 headingLora text-center" id="category">{{ $results->name }}</h1>
        <br>
        <div>
            <h4 class="mb-2 subheading text-center">Search with Uunzi to find the best {{ $results->name }} company for you.
                <br>
                <h4 class="text-center" style="color:#5cc6d0;">Use our search facility to find the cheapest loans or the
                    best loan for you</h4>

        <br>

        <b><h5><center>Please
            note that we are a comparison website and we do not offer loans or the service listed on our website.
            We simply showcase what is on the market.</center></h5></b>

            </h4>
        </div>
        <div class="container">
            <br>

            @if (count($results->innovators)>0)

                @if( ($results->name  == "Loans") || ($results->name  == "Quick Loans")|| ($results->name  == "Emergency Loans"))
                    <div>
                        <hr>

                        <div class="row">

                            <div class="col-sm-12 row">


                                <div class="col-sm-3 ">
                                    <span class="example" style="margin:auto;max-width:300px">
                                        <input type="text" class="subheading" placeholder="Enter Loan Amount..."
                                               name="search2"
                                               id="max_amount" style="width: 100%;">
                                        {{--<button type="submit" disabled><i class="fa fa-search"--}}
                                        {{--style="width:20%"></i></button>--}}
                                    </span>
                                    {{--<span class="headingLora">Loan amount:</span><span class="subheading"--}}
                                    {{--style="margin-left: 10px"> <input--}}
                                    {{--type="text" id="max_amount"--}}
                                    {{--style="border: 2px solid #607d8b;width: 100px;border-radius: 15px;"--}}
                                    {{--></span>--}}
                                </div>


                                <div class="col-sm-4">



                                         <span style="width:50%; float: left; ;
                                                  background: #5cc6d0;font-size: 17px;">
                                                <select name="" id="interest_type" style="width:100%; padding: 10px">
                                             <option value="Monthly Base">Monthly</option>
                                             <option value="Annually">Annually</option>
                                             </select>
                                            </span>

                                    <span class="example" style="margin:auto;max-width:100px">
                                                  <input type="text" class="subheading"
                                                         placeholder="Enter Interest Rate..."
                                                         name="search2"
                                                         id="interest_rate_max" style="width:50%;float: right !important;">

                                    </span>


                                    {{--<span class="headingLora">Interest rate:</span><span class="subheading"--}}
                                    {{--style="margin-left: 10px">--}}
                                    {{--<select--}}
                                    {{--name="" id="interest_type"--}}
                                    {{--style="border: 2px solid #607d8b;width: 66px ;border-radius: 15px; padding-left: 5px">--}}
                                    {{--<option value="Monthly Base">Monthly</option>--}}
                                    {{--<option value="Annually">Annually</option>--}}
                                    {{--</select>--}}

                                    {{--<input type="text" id="interest_rate_max"--}}
                                    {{--style="border: 2px solid #607d8b; width: 40px; border-radius: 15px;"--}}
                                    {{-->--}}
                                    {{--</span>--}}
                                </div>
                                <div class="col-sm-5">
                                             <span style="width:30%; float: left;  ;
                                                  background: #5cc6d0;font-size: 17px;">
                                                <select name="" id="repayment_type" style="width:100%;padding: 10px">
                                             <option value="day">Days</option>
                                             <option value="month">Months</option>
                                             </select>
                                            </span>
                                    <span class="example" style="margin:auto;max-width:300px">
                                        <input type="text" class="subheading" placeholder="Enter Repayment Period..."
                                               name="search2"
                                               id="max_repayment" style="width: 50%">
                                       <button id="ok_filter" style="width:20%"><i class="fa fa-search"
                                           ></i></button>
                                    </span>
                                    {{--<span class="headingLora">Repayment period:</span><span class="subheading"--}}
                                    {{--style="margin-left: 10px"><select--}}
                                    {{--name=""--}}
                                    {{--id="repayment_type"--}}
                                    {{--style="border: 2px solid #607d8b;width: 60px ;border-radius: 15px; padding-left: 8px">--}}
                                    {{--<option value="day">Days</option>--}}
                                    {{--<option value="month">Months</option>--}}
                                    {{--</select>--}}
                                    {{--<input type="text" id="max_repayment"--}}
                                    {{--style="border: 2px solid #607d8b;width: 100px;border-radius: 15px;"--}}
                                    {{-->--}}
                                    {{--</span>--}}

                                    {{--<button class="button"--}}
                                    {{--style="border-radius: 15px; padding: 1px 20px 1px 20px"--}}
                                    {{--id="ok_filter">Submit--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-2">--}}
                                    {{--Review:<select name="" id="review_id" style="border: 2px solid #607d8b;width: 60px">--}}
                                    {{--<option value="all">----All----</option>--}}
                                    {{--<option value="0.0">0-Star</option>--}}
                                    {{--<option value="1.0">1-Star</option>--}}
                                    {{--<option value="2.0">2-Star</option>--}}
                                    {{--<option value="3.0">3-Star</option>--}}
                                    {{--<option value="4.0">4-Star</option>--}}
                                    {{--<option value="5.0">5-Star</option>--}}

                                    {{--</select>--}}
                                    {{--<button class="button" id="ok_review">OK</button>--}}
                                    {{--</div>--}}

                                </div>
                            </div>
                        </div>
                        <hr>


                        @endif


                        <center>
                            <div class="loader" id="loading_id"></div>
                        </center>


                        <div class="row" id="innovations_div">


                        </div>

                        @else
                            <center><h1>We could not find any tech solutions in {{ $results->name }}. Please
                                    check back
                                    later</h1></center>
                        @endif
                    </div>
        </div>

    </section>
@endsection
@if( $results->name  == "Loans" || $results->name  == "Quick Loans" || $results->name  == "Emergency Loans")
@section('disclaimer')
    <hr width="100%">
    <br>
    <v-flex color="#6B66D9C" white--text xs12>

        <p class="align-content-center">"Please read all terms and conditions carefully, before committing to any
            financial arrangements to
            avoid court action, repossession. Please note that you must make loan repayments as stated by each
            company and failure to do so is a breach of contract between you and the company.
            Uunzi is not liable for any disputes between users and loan companies."</p>
        <br>
        <br>
    </v-flex>
@endsection
@endif
@section('scripts')

    <script>
        $(document).ready(function () {
            $("#loading_id").hide();

            $('#ok_filter').click(function () {

                $("#ok_filter").attr("disabled", "disabled")


                var max_val = $('#max_amount').val();


                var interest_rate_max = $('#interest_rate_max').val();
                var interest_type = $('#interest_type').val();


                var max_repayment = $('#max_repayment').val();
                var repayment_type = $('#repayment_type').val();

                if (max_repayment != '') {
                    if (repayment_type == "month") {
                        max_repayment = max_repayment * 30;
                    }
                }

                var category = $('#category').html();


                //Check Interest percentage values



                if (interest_rate_max != '') {


                    var decimal = /^[-+]?[0-9]+\.[0-9]+$/;
                    if ((interest_rate_max.match(decimal))) {
                        // alert("ok");
                    } else {
                        interest_rate_max = interest_rate_max + ".0";
                        // alert(interest_rate_max);
                    }
                }


                $('#innovations_div').hide();
                $('#innovations_div').empty();
                $("#loading_id").show();
                // alert("max_val" + max_val + "interest_rate_max" + interest_rate_max + "repayment_type" + repayment_type + "max_repayment" + max_repayment + "repayment_type" + repayment_type);
                $.ajax({

                    type: "GET",
                    url: "/innovator-subcategories-min-max-index",
                    data: {
                        'interest_rate_value': interest_rate_max,
                        'interest_type': interest_type,


                        'repayment_time': max_repayment,
                        'repayment_type': repayment_type,


                        'loan_value': max_val,
                        'category': category
                    },

                    success: function (response) {
                        // console.log(response);
                        $("#loading_id").hide();

                        $('#innovations_div').html(response);
                        $('#innovations_div').slideDown(1000);
                        $("#ok_filter").removeAttr("disabled")


                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        alert('Error - ' + errorMessage);
                    }
                });

            });

            var category = $('#category').html();

            if (category == "Accounting &amp; Bookkeeping") {
                category = "Accounting & Bookkeeping";
            }
            $.ajax({

                type: "GET",
                url: "/innovator-subcategories-min-max-index",
                data: {
                    'interest_rate_value': 0,
                    'interest_type': 0,


                    'repayment_time': 0,
                    'repayment_type': 0,


                    'loan_value': 0,
                    'category': category
                },

                success: function (response) {
                    // console.log(response);
                    $("#loading_id").hide();

                    $('#innovations_div').html(response);
                    $('#innovations_div').slideDown(1000);
                    $("#ok_filter").removeAttr("disabled")


                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    alert('Error - ' + errorMessage);
                }
            });

            // $('#ok_review').click(function () {
            //
            //     var review_id = $('#review_id').val();
            //     var category = $('#category').html();
            //     if (review_id != '') {
            //         $.ajax({
            //             type: "GET",
            //             url: "/innovator-subcategories-review-index",
            //             data: {
            //                 'review_id': review_id,
            //                 'category': category
            //             },
            //
            //             success: function (response) {
            //                 // console.log(response);
            //
            //                 $('#innovations_div').hide();
            //                 $('#innovations_div').empty();
            //                 $('#innovations_div').html(response);
            //                 $('#innovations_div').slideDown(1000);
            //
            //
            //             },
            //             error: function (xhr, ajaxOptions, thrownError) {
            //                 alert(xhr.responseText);
            //             }
            //         });
            //     }
            //     else {
            //         alert('Please Select the review value');
            //     }
            // });
            //
            // $('#ok_repayment').click(function () {
            //
            //     var min_repayment = $('#min_repayment').val();
            //     var max_repayment = $('#max_repayment').val();
            //     var category = $('#category').html();
            //     if (min_repayment != '' && min_repayment != '') {
            //         $.ajax({
            //             type: "GET",
            //             url: "/innovator-subcategories-repayment-index",
            //             data: {
            //                 'min_repayment': min_repayment,
            //                 'max_repayment': max_repayment,
            //                 'category': category
            //             },
            //
            //             success: function (response) {
            //                 // console.log(response);
            //
            //                 $('#innovations_div').hide();
            //                 $('#innovations_div').empty();
            //                 $('#innovations_div').html(response);
            //                 $('#innovations_div').slideDown(1000);
            //
            //
            //             },
            //             error: function (xhr, ajaxOptions, thrownError) {
            //                 alert(xhr.responseText);
            //             }
            //         });
            //     }
            //     else {
            //         alert('Please Select the review value');
            //     }
            // });

            $('body').on('click', '.myBox', function () {
                console.log("Function11");

                window.location = $(this).find("a").attr("href");

            });
            $('.myBox').on('click', function () {
                console.log("Function");
                window.location = $(this).find("a").attr("href");

            });
        })
        ;

    </script>

@endsection
