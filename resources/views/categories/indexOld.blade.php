@extends('layouts.app')
@section('title', $results->name)
@section('description', $results->name)
@section('content')
    <section data-banner-section="">
        <v-card flat tile>
            <v-img src="/images/uunzi-main-image-blue.png" max-height="350px">
                <v-layout column align-center class="white--text">
                    <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>
                    <h1 class="white--text mb-2 headingLora text-center">{{ $results->name }}</h1>
                    <bread-crumbs :itemms="[
                            {
                                text: 'Uunzi',
                                disabled: false,
                                href: '/',
                            },
                            {
                                text: 'Categories',
                                disabled: true,
                                href: '/category',
                            },
                            {
                                text: '{{ $results->name }}',
                                disabled: true,
                                href: '/categories/{{ $results->name }}'
                            }]">
                    </bread-crumbs>
                </v-layout>
            </v-img>
        </v-card>
    </section>
    <section data-innovators-section="">
        <v-layout ma-5 pa-5 wrap align-center justify-center>
            <v-flex xs12>
                <h1 style="color:#ff8236;" class="mb-2 headingLora text-center">{{ $results->name }}</h1>
            </v-flex>
            @if (count($results->innovators)>0)
                @foreach ($results->innovators as $innovator)
                    <v-flex xs10 sm10 md10 lg10 ma-1>
                        <v-card href="/innovators/{{ $innovator->slug }}" flat class="ma-2" hover height="300">

                            <span style="padding-top: 5px;">
                                  <span style="color:white; text-shadow: 0px 0px 17px rgb(19, 18, 18)"
                                        class="ml-1 title">{{ str_limit($innovator->company_name, 23, '') }}</span>

                            </span>
                            <span style="padding-top: 10px;">

                                <v-img height="120" width="125" src="/storage/{{ $innovator->logo }}">
                                </v-img>
                            </span>


                            <v-card-text>
                                <v-row align="center">
                                    <span class="headline condensed-light"> {{ $innovator->averageRating() }}% </span>

                                    <v-rating :size="20" :value="{{ $innovator->averageRating() }}" color="amber"
                                              half-increments dense :size="26"
                                              readonly></v-rating>

                                    <div class="subtitle-1 grey--text ml-4">{{ $innovator->totalReviews() }}</div>
                                    <h3 style="color:#ff8236;" class="text-center">{{ $innovator->company_name }}</h3>
                                    
                                    <blockquote>
                                        {{ $innovator->company_tagline }}
                                    </blockquote>
                                </v-row>
                            </v-card-text>
                        </v-card>
                    </v-flex>
                @endforeach
            @else
                <h1>We could not find any tech solutions in {{ $results->name }}. Please check back <later class="">         </later></h1>
            @endif
        </v-layout>
    </section>
@endsection
@if( $results->name  == "Loans")
@section('disclaimer')
    <hr width="100%">
    <br>
    <v-flex color="#6B66D9C" white--text xs12>

        <p class="align-content-center">"Please read all terms and conditions carefully, before committing to any
            financial arrangements to
            avoid court action, repossession. Please note that you must make loan repayments as stated by each
            company and failure to do so is a breach of contract between you and the company.
            Uunzi is not liable for any disputes between users and loan companies."</p>
        <br>
        <br>
    </v-flex>
@endsection
@endif
