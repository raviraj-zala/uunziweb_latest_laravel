@extends('layouts.app')
@section('title', 'About Uunzi')
@section('description', 'About us -- Uunzi')
@section('content')
    <section data-banner-section="">
        <v-card flat tile>
            <v-img src="/images/AboutUs.jpg" max-height="600px">
                <v-layout column align-center class="white--text">
                    {{--<v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>--}}
                    {{--<h1 class="white--text mb-2 display-1 text-center">About Us</h1>--}}
                    {{--<bread-crumbs :itemms="[--}}
                    {{--{--}}
                        {{--text: 'Uunzi',--}}
                        {{--disabled: false,--}}
                        {{--href: '/',--}}
                    {{--},--}}
                    {{--{--}}
                        {{--text: 'About Us',--}}
                        {{--disabled: true,--}}
                        {{--href: '/about-us'--}}
                    {{--}]">--}}
                    {{--</bread-crumbs>--}}
                </v-layout>
            </v-img>
        </v-card>
    </section>

    <section>
        <v-layout justify-center>
            <v-flex xs8 md8 lg8 >
                <v-img class="myImg" src="/images/OurMission.jpg" alt="AboutUs" title="AboutUs" height="100%" width="100%" contain></v-img>
            </v-flex>
        </v-layout>
        <v-layout justify-center>
            <v-flex xs8 md8 lg8 >

                <v-img class="myImg" src="/images/OurHistory.jpg" alt="AboutUs" title="AboutUs" height="100%" width="100%" contain></v-img>
            </v-flex>
        </v-layout>

        <v-layout justify-center>
            <v-flex xs8 md8 lg8 >
                <v-img class="myImg" src="/images/OurPromise.jpg" alt="AboutUs" title="AboutUs" height="100%" width="100%" contain></v-img>
            </v-flex>
        </v-layout>
    </section>
@endsection