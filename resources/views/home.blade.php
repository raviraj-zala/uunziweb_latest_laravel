@extends('layouts.app')
@section('title', Auth::user()->name)

@section('content')
<v-container grid-list-md fluid>
    <v-container grid-list-md fluid>
        <v-layout align-center justify-center row fill-height>
            <v-flex md6>
                <v-card flat class="pa-md-6">
                    <v-list>

                        <v-list-item link>
                            <v-list-item-avatar>
                                <v-img src="https://www.gravatar.com/avatar/{{  Auth::user()->gravatar() }}"></v-img>
                            </v-list-item-avatar>
                            <v-list-item-content>
                                <v-list-item-title class="title">{{ Auth::user()->name }}</v-list-item-title>
                                <v-list-item-subtitle>{{ Auth::user()->email }}</v-list-item-subtitle>
                            </v-list-item-content>

                            <v-list-item-action>
                                <v-icon>mdi-menu-down</v-icon>
                            </v-list-item-action>
                        </v-list-item>

                        <v-list-item>
                            <v-list-item-content>
                                <v-list-item-title class="title">Referals</v-list-item-title>
                                <v-list-tile-sub-title>Below is your referal link</v-list-tile-sub-title>
                                <v-text-field hint="Share this link to invite others" readonly
                                    value="{{ route('register').'/?ref='. Auth::user()->referal_code }}"></v-text-field>
                            </v-list-item-content>

                            <v-list-item-action>
                                <share-button title="Join uunzi today" text=""
                                    url="{{ route('register').'/?ref='. Auth::user()->referal_code }}"
                                    apiurl="{{ 'register/?ref='. Auth::user()->referal_code }}"></share-button>
                            </v-list-item-action>
                        </v-list-item>
                    </v-list>
                </v-card>
            </v-flex>
        </v-layout>
    </v-container>
</v-container>
@endsection