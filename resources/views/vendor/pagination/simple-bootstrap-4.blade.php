@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <v-btn color="yellow accent-4" disabled aria-disabled="true">
                @lang('pagination.previous')
            </v-btn>
        @else
            
                <v-btn color="yellow accent-4" link class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</v-btn>
        
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            
                <v-btn color="yellow accent-4" class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</v-btn>
        
        @else
            <v-btn color="yellow accent-4" disabled aria-disabled="true">
                @lang('pagination.next')
            </v-btn>
        @endif
    </ul>
@endif
