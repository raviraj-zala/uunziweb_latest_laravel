@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <v-btn disabled color="info" fab><v-icon>mdi-chevron-left</v-icon></v-btn>
        @else
            <v-btn fab color="info" link  href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><v-icon>mdi-chevron-left</v-icon></v-btn>   
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <v-btn color="info" link  disabled aria-disabled="true"><span >{{ $element }}</span></v-btn>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <v-btn color="info" link disabled ><span >{{ $page }}</span></v-btn>
                    @else
                        <v-btn color="info" link   href="{{ $url }}">{{ $page }} </v-btn>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <v-btn color="info" fab  href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><v-icon>mdi-chevron-right</v-icon></v-btn> 
        @else
            <v-btn fab color="info" link  disabled aria-disabled="true" aria-label="@lang('pagination.next')">
                <v-icon>mdi-chevron-right</v-icon>
            </v-btn>
        @endif
    </ul>
@endif
