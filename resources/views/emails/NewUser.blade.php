@component('mail::message')
# A new user Just joined Uunzi

This mail is to inform you that {{ $user->name }} of email
{{$user->email}}
has just signed up. 
You can view more in the admin dashboard
@endcomponent
