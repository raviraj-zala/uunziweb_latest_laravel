@extends('layouts.app')
@section('title', 'Tech and Innovation Marketplace')
@section('description', 'Uunzi is a location-based innovation marketplace that connects people to local tech solutions.
With Uunzi you can search, compare and review.
Whatever you’re looking for, just search with Uunzi!')
@section('content')
    <section data-banner-section="">


        <v-card flat tile>
            <v-img src="/images/uunzi-main-image-blue.png" max-height="600px">
                <v-layout column align-center class="white--text">
                    <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="180" contain></v-img>
                    <h1 class="white--text mb-2 headingLora text-center">Are you looking for apps, start-ups or tech solutions?</h1>
                    <div class="subheading ma-5 text-center">Whatever you are looking for, just search with Uunzi!</div>
                </v-layout>


                <v-form method="GET" action="/search" id="searchForm" style="margin-bottom: 0px;">
                    <v-container grid-list-xl>
                        <v-layout justify-center align-content-center>
                            <v-flex xs12 sm12 md6>
                                <v-text-field height="60px" color="#00c6cf" name="query"
                                              label="What are you looking for?" rounded solo
                                              flat prefix="Search:" append-icon="mdi-magnify" @click:append="search()">

                                </v-text-field></v-flex>
                        </v-layout>
                    </v-container>
                    <input type="hidden" name="country" value="{{ $country }}">
                </v-form>

                <section data-innovators-new-section="">
                    <v-layout wrap align-center justify-center>
                        <v-flex xs12>
                            {{--<h1 style="color:#ff8236;" class="mb-2 display-1 text-center">Discover Categories</h1>--}}
                            {{--<div style="color: #ffffff;" class="subheading text-center">Discover the available categories</div>--}}
                        </v-flex>
                        @if (count($categories) > 0)
                            @foreach ($categories as $category)
                                <v-btn rounded light dark color="grey" class="text-capitalize ma-1"
                                       href="/category/{{ $category->name }}" hover>
                                    <div style="color: #ffffff ">{{ $category->name }}</div>
                                </v-btn>
                            @endforeach

                            <v-btn active rounded light dark color="#f58634" class="text-capitalize ma-1"
                                   href="/category" hover>
                                See More Categories
                            </v-btn>
                        @else
                            <h4 style="color: #ffffff;">No categories are available yet!</h4>
                        @endif
                    </v-layout>
                </section>
            </v-img>
        </v-card>
    </section>

    <section data-innovators-new-section="">
        <v-layout ma-5 pa-5 wrap align-center justify-center>
            <v-flex xs12>
                <h1 style="color:#ff8236;" class="mb-2 headingLora text-center">New tech solutions in {{ $country }}</h1>
                <div style="color: #00c6cf;" class="subheading text-center">Discover all the newly added tech solutions
                </div>
            </v-flex>

            @if (count($latestInnovators) > 0)
                @foreach ($latestInnovators as $innovator)
                    <v-flex xs12 sm4 md6 lg2 ma-1>
                        <v-card href="/innovators/{{ $innovator->slug }}" hover height="300">
                            <center style="padding-top: 5px;">
                                  <span style="color:white; text-shadow: 0px 0px 17px rgb(19, 18, 18)"
                                        class="ml-1 title">{{ str_limit($innovator->company_name, 23, '') }}</span>

                            </center>
                            <center style="padding-top: 10px;">

                                <v-img height="120" width="125" src="/storage/{{ $innovator->logo }}">
                                    <v-col align="end">

                                    </v-col>
                                </v-img>
                            </center>

                            <v-list two-line>
                                <v-list-item>
                                    <v-list-item-title>{{ $innovator->company_name }}</v-list-item-title>
                                    <v-list-item-subtitle>{{ $innovator->company_tagline }}</v-list-item-subtitle>
                                </v-list-item>
                            </v-list>

                            <v-card-text align="center">
                                <v-row align="center">
                                    <v-rating :value="{{ $innovator->averageRating() }}" color="amber" half-increments
                                              dense readonly
                                              :size="16"></v-rating>
                                    <span class="body-2 condensed-light"> {{ $innovator->averageRating() }}
                                        ({{ $innovator->totalReviews() }})
                                     </span>
                                </v-row>
                            </v-card-text>
                        </v-card>
                    </v-flex>
                @endforeach
            @else
                <h5>We could not find any innovations from {{ $country }} Please check back later</h5>
            @endif
        </v-layout>
        <v-layout justify-center>
            <v-flex xs12 sm4 md6 lg2 ma-1>
                <center>
                    <v-btn href="/search/?country={{ $country }}" color="success">See More Innovations</v-btn>
                </center>
            </v-flex>
        </v-layout>
    </section>



    <section data-innovators-section="">
        <v-layout ma-5 pa-5 wrap align-center justify-center>
            <v-flex xs12>
                <h1 style="color:#ff8236;" class="mb-2 headingLora text-center">More tech solutions
                    from {{ $country }}</h1>
                <div style="color: #00c6cf;" class="subheading text-center">To Discover tech solutions
                    from {{ $country }} </div>
            </v-flex>
            @if (count($innovators)>0)
                @foreach ($innovators as $innovator)
                    <v-flex xs12 sm4 md6 lg2 ma-1>
                        <v-card href="/innovators/{{ $innovator->slug }}" hover height="300">
                            <center style="padding-top: 5px;">
                                <span style="color:white; text-shadow: 0px 0px 17px rgb(19, 18, 18)"
                                      class="ml-1 title">{{ str_limit($innovator->company_name, 23, '') }}</span>

                            </center>

                            <center style="padding-top: 10px;">
                                <v-img height="120" width="125" src="/storage/{{ $innovator->logo }}">
                                    <v-col align="end">

                                    </v-col>
                                </v-img>
                            </center>


                            <v-list two-line>
                                <v-list-item>
                                    <v-list-item-title>{{ $innovator->company_name }}</v-list-item-title>
                                    <v-list-item-subtitle>{{ $innovator->company_tagline }}</v-list-item-subtitle>
                                </v-list-item>
                            </v-list>
                            <v-card-text>
                                <v-row align="center">
                                    <v-rating :value="{{ $innovator->averageRating() }}" color="amber" half-increments
                                              dense readonly
                                              :size="16"></v-rating>
                                    <span class="body-2 condensed-light"> {{ $innovator->averageRating() }}
                                        ({{ $innovator->totalReviews() }})
            </span>
                                </v-row>
                            </v-card-text>
                        </v-card>
                    </v-flex>
                @endforeach
            @else
                <h5>We could not find any innovations from {{ $country }} Please check back later</h5>
            @endif
        </v-layout>
        <v-layout justify-center>
            <v-flex xs12 sm4 md6 lg2 ma-1>
                <center>
                    <v-btn href="/search/?country={{ $country }}" color="success">See More Innovations</v-btn>
                </center>
            </v-flex>
        </v-layout>
    </section>

    <section data-how-it-works="">
        <v-layout ma-5 pa-5 wrap align-center justify-center my-2>
            <v-flex xs12>
                <h3 style="color:#ff8236;" class="ma-2 headingLora text-center">How Uunzi works</h3>
                <div style="color: #00c6cf;" class="subheading text-center">Are you searching for an app, start up or tech?
                </div>
            </v-flex>


            <v-flex xs12 sm12 md3 tile>
                <v-card hover flat class="ma-2">
                    <v-img src="/images/search-online.svg" max-height="200" contain></v-img>
                    <h3 class="text-center">Find Innovators</h3>
                    <v-card-text class="text-justify">
                        Use the search bar to find apps, startups or desired tech.  
                        For a quick search click on your desired categories to discover tech in those categories.
                    </v-card-text>
                </v-card>
            </v-flex>
            <v-flex xs12 sm12 md3 tile>
                <v-card hover flat class="ma-1">
                    <v-img src="/images/find-listing.svg" max-height="200" contain></v-img>
                    <h3 class="text-center">Review technology</h3>
                    <v-card-text class="text-justify">
                        Find apps, start-up or desired tech and get all the information available.
                    </v-card-text>
                </v-card>
            </v-flex>

            <v-flex xs12 md3 tile>
                <v-card hover flat class="ma-1">
                    <v-img src="/images/make-online-booking.svg" max-height="200" contain></v-img>
                    <h3 class="text-center">Connect with them </h3>
                    <v-card-text class="text-justify">
                        Once you have found the perfect app, start up or tech, you can visit the website directly from our website.
                        <a href="innovator-subcategories">Today</a>

                    </v-card-text>
                </v-card>
            </v-flex>
        </v-layout>
    </section>

    <v-card img="/images/b.jpg" height="400">
        <v-layout column align-center justify-center fill-height class="white--text">
            <h1 class="white--text mb-2 headingLora text-center">Get business exposure</h1>
            <div class="subheading text-center">Get your business featured today</div>
            @guest
                <v-btn large dark @click="auth_dialog = true" color="primary">Learn More</v-btn>
            @endguest


        </v-layout>
    </v-card>

    <v-layout wrap align-center justify-center my-2 ma-5>
        <v-flex xs12 class="ma-5 pa-5">
            <h1 style="color:#ff8236;" class="mb-2 headingLora text-center">Uunzi Community</h1>
            <div style="color: #00c6cf;" tile class="subheading text-center">Tech talk with Uunzi community – get
                involved!
            </div>
            <div class="text-center subheading">
                Africa is experiencing a digital renaissance revolutionising the way we use technology to travel, <br>
                socialise, buy and sell.
                We have become the leaders of homegrown, tech-driven innovations that transforms lives. <br>
                If you are interested in new disruptive technology and innovation solutions, join our
                community <a
                        href="/uunzi-community">here</a>
            </div>
        </v-flex>

        @foreach ($posts as $post)

            <v-flex xs12 sm12 lg2 md6>
                <v-card href="{{ $post->slug }}" class="ma-3" hover flat max-height="300" tile>
                    <v-img src="{{ asset('storage/'.$post->image) }}" height="125">
                        <div class="ma-1">
          <span style="color:white; text-shadow: 0px 0px 17px rgb(19, 18, 18)"
                class="headingLora white--text font-weight-light font-weight-medium">{{ str_limit($post->title, 20, '...') }}</span>
                        </div>
                    </v-img>
                    <div class="pa-2">
                        <div class="text-center">
          <span class="subtitle-1 font-weight-regular">{{ date('M d, Y', strtotime( $post->published_at)) }} •
              @foreach($post->tags as $tag) {{  str_limit($tag->name  ,10, '...') }} @endforeach </span>
                        </div>

                        <span class="subheading font-weight-light" height="20px">
          {{ str_limit($post->description, 20, '...') }}
        </span>
                    </div>
                </v-card>
            </v-flex>
        @endforeach
    </v-layout>

    <v-layout justify-center class="my-5">
        <v-flex xs12 md8>
            <div style="color:#ff8236;" class="mb-2 headingLora text-center">List with us</div>
            <div class="text-center ma-5 pa-5 subheading">
                If you are a registered start- up or entrepreneur with a disruptive ready-to-market technology
                or innovative solution and would like to list on Uunzi - contact <a href="mailto:info@uunzi.com">info@uunzi.com</a>
                or alternatively list
                with us <a
                        @click="auth_dialog = true">here</a>
            </div>
        </v-flex>
    </v-layout>
@endsection
