@extends('layouts.app')
@section('title', $innovator->company_name)
@section('description', $innovator->product_information)
@section('image', asset('storage/'.$innovator->logo))
<style>
    div.sticky {
        position: -webkit-sticky; /* Safari */
        position: sticky !important;
        top: 0;
    }
</style>


@section('content')
    <v-container grid-list-md fluid>
        <v-layout row wrap>
            @if($innovator->subCategory->name == 'Loans' || $innovator->subCategory->name == 'Quick Loans')
                <v-flex xs12 sm3 md3 lg3>

                    {{--<div class="sticky" style="padding-top: 100px !important;">--}}
                    <div class="sticky">
                        <v-flex xs12 lg12 md12>
                            <h3 style="color:#607d8b;" class="mb-2 headingLora text-center">Loan Calculator</h3>
                        </v-flex>
                        <loan-calculator-component></loan-calculator-component>
                    </div>


                </v-flex>
                <v-flex xs12 sm6 md6 lg6>
                    <innovator-component v-on:noauth="auth_dialog = true" :auth="{{ Auth::check() ? 'true' : 'false' }}"
                                         :innovator="{{ $innovator }}"></innovator-component>
                </v-flex>


            @else

                <v-flex xs12 sm8 md8 lg8 pa-10>
                    <innovator-component v-on:noauth="auth_dialog = true" :auth="{{ Auth::check() ? 'true' : 'false' }}"
                                         :innovator="{{ $innovator }}"></innovator-component>
                </v-flex>
            @endif
            <v-flex xs12 sm3 md3 lg3>
                <v-flex xs12 lg12 md12>
                    <h3 style="color:#607d8b;" class="mb-2 headingLora text-center">Compare {{$innovator->company_name}} With</h3>
                </v-flex>
                @foreach($similar_categories as $inovator)

                    <compare-innovator-component :inovator="{{$inovator}}"
                    ></compare-innovator-component>

                @endforeach

                <v-layout justify-center>
                    <v-flex xs12 md10 lg10>


                        <v-card class="pa-4" xs12 md12 lg12>
                            <center>
                                <v-btn  href="/search/?country={{$country}}"
                                       color="success">
                                    <v-card-text>
                                        See More Companies
                                    </v-card-text>

                                </v-btn>
                            </center>

                        </v-card>

                        <br>

                    </v-flex>

                </v-layout>
            </v-flex>
        </v-layout>
    </v-container>
@endsection
@if( $innovator->subCategory->name  == "Loans" || $innovator->subCategory->name  == "Quick Loans" || $innovator->subCategory->name  == "Emergency Loans")

@section('disclaimer')
    <hr width="100%">
    <br>
    <v-flex color="#6B66D9C" white--text xs12>

        <p class="align-content-center">"Please read all terms and conditions carefully, before committing to any
            financial arrangements to
            avoid court action, repossession. Please note that you must make loan repayments as stated by each
            company and failure to do so is a breach of contract between you and the company.
            Uunzi is not liable for any disputes between users and loan companies."</p>
        <br>
        <br>
    </v-flex>
@endsection
@endif
