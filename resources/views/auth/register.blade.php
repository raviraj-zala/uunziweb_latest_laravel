@extends('layouts.app')

@section('content')
<v-container grid-list-md fluid>
  <v-layout align-center justify-center row fill-height>
    <v-flex md6>
      <v-card flat class="pa-md-6">
        <v-card-text>
          <register-component csrf="{{ csrf_token() }}"></register-component>
          
        </v-card-text>
      </v-card>
    </v-flex>
  </v-layout>
</v-container>
@endsection