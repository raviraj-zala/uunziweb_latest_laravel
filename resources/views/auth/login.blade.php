@extends('layouts.app')

@section('content')
<v-container grid-list-md fluid>
  <v-layout align-center justify-center row fill-height>
    <v-flex md5 ma-4>
      <v-card flat class="pa-md-6">
        <v-card-text>
          @if ($errors->any)
              @foreach ($errors->all() as $error)
                  <v-alert type="error">
                    {{ $error }}
                  </v-alert>
              @endforeach
          @endif
          <login-component csrf="{{ csrf_token() }}"></login-component>
        </v-card-text>
      </v-card>
    </v-flex>
  </v-layout>
</v-container>
@endsection