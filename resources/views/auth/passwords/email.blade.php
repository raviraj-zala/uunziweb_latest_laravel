@extends('layouts.app')

@section('content')
<v-container grid-list-md fluid>
  <v-layout align-center justify-center row fill-height>
    <v-flex md6>
      <v-card flat class="pa-md-6 mt-12">
        <v-card-text>
          @foreach ($errors->all() as $item)
          <v-alert type="error" :value>
            {{ $item }}
          </v-alert>
          @endforeach
          @if (session('status'))
          <v-alert type="success" :value="true">
            {{ session('status') }}
          </v-alert>
          @endif
          <v-form action="{{ route('password.email') }}" method="post">
            @csrf
            <v-text-field required outlined name="email" type="email" clearable label="Email Address"></v-text-field>
            <v-btn large rounded outlined color="success" type="submit">Reset Password</v-btn>
          </v-form>
        </v-card-text>
      </v-card>
    </v-flex>
  </v-layout>
</v-container>
@endsection