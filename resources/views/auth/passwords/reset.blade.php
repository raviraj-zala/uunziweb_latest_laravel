@extends('layouts.app')

@section('content')

<v-container grid-list-md fluid>
    <v-layout align-center justify-center row fill-height>
        <v-flex md6>
            <v-card flat class="pa-md-6">
                @foreach ($errors->all() as $item)
                <v-alert type="error" :value="true">
                    {{ $item }}
                </v-alert>
                @endforeach
                <v-card-title>
                    {{ __('Reset Password') }}
                </v-card-title>
                <v-card-text>
                    <v-form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <v-text-field name="email" label="{{ __('E-Mail Address') }}" id="email"
                            value="{{ $email ?? old('email') }}" autocomplete="email" autofocus type="email">
                        </v-text-field>

                        <v-text-field name="password" label="{{ __('Password') }}" id="password"
                            autocomplete="new-password" type="password"></v-text-field>

                        <v-text-field label="{{ __('Confirm Password') }}" id="password-confirm"
                            autocomplete="new-password" name="password_confirmation" type="password"></v-text-field>

                        <v-btn large rounded outlined color="success" type="submit">{{ __('Reset Password') }}</v-btn>
                    </v-form>
                </v-card-text>
            </v-card>
        </v-flex>
    </v-layout>
</v-container>
@endsection