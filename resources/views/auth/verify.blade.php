@extends('layouts.app')

@section('content')
<v-container grid-list-md fluid>
    <v-layout align-center justify-center row fill-height>
        <v-flex md6>
            <v-card flat class="pa-md-6">
                <v-card-title>
                    {{ __('Verify Your Email Address') }}
                </v-card-title>
                <v-card-text>
                    @if (session('resent'))
                    <v-alert type="success" :value="true">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                    </v-alert> 
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a
                        href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </v-card-text>
            </v-card>
        </v-flex>
    </v-layout>
</v-container>
@endsection