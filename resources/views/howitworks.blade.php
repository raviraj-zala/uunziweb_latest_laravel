@extends('layouts.app')
@section('title', 'Frequently asked questions')
@section('description', 'All frequently asked questions about uunzi')
@section('content')
<section data-banner-section="">
    <v-card flat tile>
        <v-img src="/images/uunzi-main-image-blue.png" max-height="350px">
            <v-layout column align-center class="white--text">
                <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>
                <h5 class="white--text mb-2 headingLora text-center">Frequently asked questions</h5>
                <bread-crumbs :itemms="[
                    {
                        text: 'Uunzi',
                        disabled: false,
                        href: '/',
                    },
                    {
                        text: 'FAQs',
                        disabled: true,
                        href: '/faqs'
                    }]">
                </bread-crumbs>
            </v-layout>
        </v-img>
    </v-card>
</section>

<section>
    <v-layout wrap justify-center>
        <v-flex xs12 md8>
            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">What is Uunzi?</h5>
                <p>
                    Uunzi is a platform that connects users to new and exciting start-ups and technology companies that
                    are local to you. With Uunzi, you can search, compare and review exciting companies. We source local
                    companies on your behalf saving you time
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">Why Uunzi?</h5>
                <p>
                    Technology plays a critical part in our lives and will continue to be at the forefront of
                    development. With so many, apps, technology companies and solutions available, we want to ensure
                    that everybody has information and access to new innovations that are situated locally so you don’t
                    miss out.
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">Do you offer loans at Uunzi?
                </h5>
                <p>
                    No, we do not offer loans, we showcase companies that offer a variety of services including loans
                    and alternative financial services.
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">Do you offer the services you are listed on the
                    website?
                </h5>
                <p>
                    We are an independent company that advertise and list technology companies so users have a
                    comprehensive list of relevant information and are able to connect with their preferred companies.
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">What countries do you work in?
                </h5>
                <p>
                    We are currently piloting Uunzi in Kenya and will scale in other African countries and Europe.
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">How do you know the legitimacy of companies
                    advertised?
                </h5>
                <p>
                    We do a thorough check on the companies that are listed on our website to ensure the companies are
                    legitimate companies, also our review system allows you to see what people think about individual
                    companies. However, each individual must exercise due diligence before engaging in any financial
                    transaction.
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">What if the company/ technology company doesn’t
                    deliver as promised?

                </h5>
                <p>
                    As a third party, we are unable to step in, however, if we are notified of the issue, an internal
                    audit will be conducted and the company will be removed from using Uunzi platform.
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">Do you only showcase Fintech technology
                    companies?</h5>
                <p>
                    No, currently we are showcasing Fintech but will be showcasing companies in other sectors in the
                    near future. – sign up to our newsletter to find out more!
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">How do I advertise my company on Uunzi?</h5>
                <p>
                    To advertise sign up as an innovator, fill out all business information as stated in the online
                    form, you should be approved within 24-48 hours
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">How do I sign up as a user</h5>
                <p>
                    Sign up on our website, those who sign up will have access to updates and news of new and upcoming
                    technology solutions.
                </p>
            </div>

            <div class="text-start ma-5 pa-5 body-1">
                <h5 style="color:#ff8236;" class="mb-2 headingLora text-start">How do I contact Uunzi if I have any concerns?
                </h5>
                <p>
                    You can contact us on <a href="mailto:info@uunzi.com">info@uunzi.com</a> or alternatively via
                    Whatsapp on <a href="tel:+254793823929">+254 793 823929</a>
                </p>
            </div>
        </v-flex>
    </v-layout>
</section>
@endsection