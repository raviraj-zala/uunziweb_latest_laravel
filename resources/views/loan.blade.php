@extends('layouts.app')
@section('title', 'Loan Calculator')
@section('description', 'Loan calculator')
@section('content')
    <section data-banner-section="">
        <v-card flat tile>
            <v-img src="/images/uunzi-main-image-blue.png" max-height="350px">
                <v-layout column align-center class="white--text">
                    <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>
                    <h1 class="white--text mb-2 headingLora text-center">About Us</h1>
                    <bread-crumbs :itemms="[
                            {
                                text: 'Uunzi',
                                disabled: false,
                                href: '/',
                            },
                            {
                                text: 'Loan Calculator',
                                disabled: true,
                                href: '/loan-calculator'
                            }]">
                    </bread-crumbs>
                </v-layout>
            </v-img>
        </v-card>
    </section>
    <section data-calculator-section>
        <loan-calculator-component></loan-calculator-component>
    </section>
@endsection
