<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description', 
    'Uunzi is a location-based innovation marketplace that connects people to local tech solutions.
     With Uunzi you can search, compare and review. Whatever you’re looking for, just search with Uunzi!')">
    <meta name="theme-color" content="#607D8B">
    <!-- COMMON TAGS -->
    <meta name="image" content="@yield('image','/images/uunzi-main-image-blue.png')">
    <link rel="shortcut icon" type="image/png" href="https://image.flaticon.com/icons/svg/73/73537.svg"/>
    <!-- Schema.org for Google -->
    <meta itemprop="description" content="@yield('description', 
    'Uunzi is a location-based innovation marketplace that connects people to local tech solutions.
     With Uunzi you can search, compare and review. Whatever you’re looking for, just search with Uunzi!')">
    <meta itemprop="image" content="@yield('image','/images/uunzi-main-image-blue.png')">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title"
          content="@yield('title', 'Tech and innovation marketplace') - {{ config('app.name', 'Laravel') }}">
    <meta name="twitter:description" content="@yield('description', 
    'Uunzi is a location-based innovation marketplace that connects people to local tech solutions.
     With Uunzi you can search, compare and review. Whatever you’re looking for, just search with Uunzi!')">
    <meta name="twitter:site" content="@uunziOfficial">
    <meta name="twitter:image:src" content="@yield('image','/images/uunzi-main-image-blue.png')">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:description" content="@yield('description', 
    'Uunzi is a location-based innovation marketplace that connects people to local tech solutions.
     With Uunzi you can search, compare and review. Whatever you’re looking for, just search with Uunzi!')">

    <meta name="og:image" content="@yield('image','/images/uunzi-main-image-blue.png')">
    <meta name="og:url" content="https://uunzi.com/">
    <meta name="og:site_name" content="Uunzi">
    <meta name="og:type" content="website">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Maifest json --}}
    <link rel="manifest" href="/manifest.json">

    <title>@yield('title', 'Tech and innovation marketplace') - {{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="{{ mix('js/manifest.js') }}" defer></script>
    <script src="{{ mix('js/vendor.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">


    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135939204-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-135939204-1');
    </script>


    <script id="mcjs">!function (c, h, i, m, p) {
            m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
        }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/bc7a6f2de76f796a78a26798d/968127048f9a677aac9f75b41.js");</script>


</head>
<style>
    [v-cloak] > * {
        display: none
    }

    [v-cloak]::before {
        content: " ";
        display: block;
        width: 40px;
        height: 40px;
        position: absolute;
        top: 50%;
        left: 45%;
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHdpZHRoPSI0MHB4IiBoZWlnaHQ9IjQwcHgiIHZpZXdCb3g9IjAgMCA0MCA0MCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bWw6c3BhY2U9InByZXNlcnZlIiBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEuNDE0MjE7IiB4PSIwcHgiIHk9IjBweCI+CiAgICA8ZGVmcz4KICAgICAgICA8c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWwogICAgICAgICAgICBALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7CiAgICAgICAgICAgICAgZnJvbSB7CiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICAgIHRvIHsKICAgICAgICAgICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoLTM1OWRlZykKICAgICAgICAgICAgICB9CiAgICAgICAgICAgIH0KICAgICAgICAgICAgQGtleWZyYW1lcyBzcGluIHsKICAgICAgICAgICAgICBmcm9tIHsKICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICAgIHRvIHsKICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKC0zNTlkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICB9CiAgICAgICAgICAgIHN2ZyB7CiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IDUwJSA1MCU7CiAgICAgICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc3BpbiAxLjVzIGxpbmVhciBpbmZpbml0ZTsKICAgICAgICAgICAgICAgIC13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuOwogICAgICAgICAgICAgICAgYW5pbWF0aW9uOiBzcGluIDEuNXMgbGluZWFyIGluZmluaXRlOwogICAgICAgICAgICB9CiAgICAgICAgXV0+PC9zdHlsZT4KICAgIDwvZGVmcz4KICAgIDxnIGlkPSJvdXRlciI+CiAgICAgICAgPGc+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMCwwQzIyLjIwNTgsMCAyMy45OTM5LDEuNzg4MTMgMjMuOTkzOSwzLjk5MzlDMjMuOTkzOSw2LjE5OTY4IDIyLjIwNTgsNy45ODc4MSAyMCw3Ljk4NzgxQzE3Ljc5NDIsNy45ODc4MSAxNi4wMDYxLDYuMTk5NjggMTYuMDA2MSwzLjk5MzlDMTYuMDA2MSwxLjc4ODEzIDE3Ljc5NDIsMCAyMCwwWiIgc3R5bGU9ImZpbGw6YmxhY2s7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNNS44NTc4Niw1Ljg1Nzg2QzcuNDE3NTgsNC4yOTgxNSA5Ljk0NjM4LDQuMjk4MTUgMTEuNTA2MSw1Ljg1Nzg2QzEzLjA2NTgsNy40MTc1OCAxMy4wNjU4LDkuOTQ2MzggMTEuNTA2MSwxMS41MDYxQzkuOTQ2MzgsMTMuMDY1OCA3LjQxNzU4LDEzLjA2NTggNS44NTc4NiwxMS41MDYxQzQuMjk4MTUsOS45NDYzOCA0LjI5ODE1LDcuNDE3NTggNS44NTc4Niw1Ljg1Nzg2WiIgc3R5bGU9ImZpbGw6cmdiKDIxMCwyMTAsMjEwKTsiLz4KICAgICAgICA8L2c+CiAgICAgICAgPGc+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMCwzMi4wMTIyQzIyLjIwNTgsMzIuMDEyMiAyMy45OTM5LDMzLjgwMDMgMjMuOTkzOSwzNi4wMDYxQzIzLjk5MzksMzguMjExOSAyMi4yMDU4LDQwIDIwLDQwQzE3Ljc5NDIsNDAgMTYuMDA2MSwzOC4yMTE5IDE2LjAwNjEsMzYuMDA2MUMxNi4wMDYxLDMzLjgwMDMgMTcuNzk0MiwzMi4wMTIyIDIwLDMyLjAxMjJaIiBzdHlsZT0iZmlsbDpyZ2IoMTMwLDEzMCwxMzApOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTI4LjQ5MzksMjguNDkzOUMzMC4wNTM2LDI2LjkzNDIgMzIuNTgyNCwyNi45MzQyIDM0LjE0MjEsMjguNDkzOUMzNS43MDE5LDMwLjA1MzYgMzUuNzAxOSwzMi41ODI0IDM0LjE0MjEsMzQuMTQyMUMzMi41ODI0LDM1LjcwMTkgMzAuMDUzNiwzNS43MDE5IDI4LjQ5MzksMzQuMTQyMUMyNi45MzQyLDMyLjU4MjQgMjYuOTM0MiwzMC4wNTM2IDI4LjQ5MzksMjguNDkzOVoiIHN0eWxlPSJmaWxsOnJnYigxMDEsMTAxLDEwMSk7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNMy45OTM5LDE2LjAwNjFDNi4xOTk2OCwxNi4wMDYxIDcuOTg3ODEsMTcuNzk0MiA3Ljk4NzgxLDIwQzcuOTg3ODEsMjIuMjA1OCA2LjE5OTY4LDIzLjk5MzkgMy45OTM5LDIzLjk5MzlDMS43ODgxMywyMy45OTM5IDAsMjIuMjA1OCAwLDIwQzAsMTcuNzk0MiAxLjc4ODEzLDE2LjAwNjEgMy45OTM5LDE2LjAwNjFaIiBzdHlsZT0iZmlsbDpyZ2IoMTg3LDE4NywxODcpOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTUuODU3ODYsMjguNDkzOUM3LjQxNzU4LDI2LjkzNDIgOS45NDYzOCwyNi45MzQyIDExLjUwNjEsMjguNDkzOUMxMy4wNjU4LDMwLjA1MzYgMTMuMDY1OCwzMi41ODI0IDExLjUwNjEsMzQuMTQyMUM5Ljk0NjM4LDM1LjcwMTkgNy40MTc1OCwzNS43MDE5IDUuODU3ODYsMzQuMTQyMUM0LjI5ODE1LDMyLjU4MjQgNC4yOTgxNSwzMC4wNTM2IDUuODU3ODYsMjguNDkzOVoiIHN0eWxlPSJmaWxsOnJnYigxNjQsMTY0LDE2NCk7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNMzYuMDA2MSwxNi4wMDYxQzM4LjIxMTksMTYuMDA2MSA0MCwxNy43OTQyIDQwLDIwQzQwLDIyLjIwNTggMzguMjExOSwyMy45OTM5IDM2LjAwNjEsMjMuOTkzOUMzMy44MDAzLDIzLjk5MzkgMzIuMDEyMiwyMi4yMDU4IDMyLjAxMjIsMjBDMzIuMDEyMiwxNy43OTQyIDMzLjgwMDMsMTYuMDA2MSAzNi4wMDYxLDE2LjAwNjFaIiBzdHlsZT0iZmlsbDpyZ2IoNzQsNzQsNzQpOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTI4LjQ5MzksNS44NTc4NkMzMC4wNTM2LDQuMjk4MTUgMzIuNTgyNCw0LjI5ODE1IDM0LjE0MjEsNS44NTc4NkMzNS43MDE5LDcuNDE3NTggMzUuNzAxOSw5Ljk0NjM4IDM0LjE0MjEsMTEuNTA2MUMzMi41ODI0LDEzLjA2NTggMzAuMDUzNiwxMy4wNjU4IDI4LjQ5MzksMTEuNTA2MUMyNi45MzQyLDkuOTQ2MzggMjYuOTM0Miw3LjQxNzU4IDI4LjQ5MzksNS44NTc4NloiIHN0eWxlPSJmaWxsOnJnYig1MCw1MCw1MCk7Ii8+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K');
    }

    .view-group {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: row;
        flex-direction: row;
        padding-left: 0;
        margin-bottom: 0;
    }

    .thumbnail {
        /*background-color: lightgray;*/
        margin-bottom: 30px;
        padding: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .item.list-group-item {
        float: none;
        width: 100%;
        background-color: #fff;
        margin-bottom: 30px;
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
        padding: 0 1rem;
        border: 0;
    }

    .item.list-group-item .img-event {
        float: left;
        width: 30%;
    }

    .item.list-group-item .list-group-image {
        margin-right: 10px;
    }

    .item.list-group-item .thumbnail {
        background-color: blue;
        margin-bottom: 0px;
        display: inline-block;
    }

    .item.list-group-item .caption {

        float: left;
        width: 70%;
        margin: 0;
    }

    .item.list-group-item:before, .item.list-group-item:after {
        display: table;
        content: " ";
    }

    .item.list-group-item:after {
        clear: both;
    }

    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 2px 6px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
    }

    .myBox {
        cursor: pointer;
    }

    .mySpan {
        cursor: pointer;
    }

    .myBox:hover {
        background-color: #fff;
        -moz-box-shadow: 3px 3px 5px 6px #ccc;
        -webkit-box-shadow: 3px 3px 5px 6px #ccc;
        box-shadow: 3px 3px 5px 6px #ccc;

    }

    .myImg {
        background-color: #fff;
        -moz-box-shadow: 3px 3px 5px 6px #ccc;
        -webkit-box-shadow: 3px 3px 5px 6px #ccc;
        box-shadow: 3px 3px 5px 6px #ccc;

    }

    .checked {
        color: orange;
    }

    .unchecked {
        color: #e3ecfb;
    }

    .fa-star {
        font-size: 15px;
        padding: 2px;
    }

    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    /* The Modal (background) */
    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 575px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        padding-right: 100px;
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 60%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    p {
        font-family: Muli;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .subheading {
        font-family: Muli !important;
        font-size: 17px;
        font-style: normal;
        font-variant: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .headingLora {
        font-family: Lora !important;
        font-size: 24px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 26.4px;
    }

    .v-application {
        font-family: Lora !important;
        font-size: 24px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 26.4px;
    }
    .v-menu__content{
        top: 64px !important;
    }
    .example input[type=text] {
        padding: 13px;
        font-size: 17px;
        border: 0px solid grey;
        float: left;

        background: #f1f1f1;
    }

    .example button {
        float: left;
        width: 20%;
        padding: 10px;
        background: #5cc6d0;
        color: white;
        font-size: 17px;
        border: 0px solid grey;
        border-left: none;

    }









</style>

<body>
<noscript>
    Javascript is required to use this website
</noscript>
<div id="app" v-cloak>
    <!-- App.vue -->

    <v-app>
        <v-navigation-drawer clipped color="#607D8B" v-model="drawer" dark temporary app>

            @auth
                <v-list>

                    <v-list-item link>
                        <v-list-item-avatar>
                            <v-img src="https://www.gravatar.com/avatar/{{  Auth::user()->gravatar() }}"></v-img>
                        </v-list-item-avatar>
                        <v-list-item-content>
                            <v-list-item-title class="title">{{ Auth::user()->name }}</v-list-item-title>
                            <v-list-item-subtitle>{{ Auth::user()->email }}</v-list-item-subtitle>
                        </v-list-item-content>

                        <v-list-item-action>
                            <v-icon>mdi-menu-down</v-icon>
                        </v-list-item-action>
                    </v-list-item>
                </v-list>
                <v-divider></v-divider>
            @endauth @guest
                <v-list>
                    <v-list-item>
                        <v-list-item-content>
                            <v-list-item-title class="title">
                                {{ config('app.name') }}
                            </v-list-item-title>
                            <v-list-item-subtitle>
                                Tech and innovation
                            </v-list-item-subtitle>
                        </v-list-item-content>
                    </v-list-item>
                </v-list>
            @endguest

            <v-divider></v-divider>

            <v-list dense nav>
                <v-list-item link href="/">
                    <v-list-item-icon>
                        <v-icon>mdi-home</v-icon>
                    </v-list-item-icon>

                    <v-list-item-content>
                        <v-list-item-title>Home</v-list-item-title>
                    </v-list-item-content>
                </v-list-item>


                <v-list-item link href="{{ url('category') }}">
                    <v-list-item-icon>
                        <v-icon>mdi-widgets</v-icon>
                    </v-list-item-icon>

                    <v-list-item-content>
                        <v-list-item-title  >Categories</v-list-item-title>
                    </v-list-item-content>
                </v-list-item>


                <v-list-item link href="{{ url('uunzi-community') }}">
                    <v-list-item-icon>
                        <v-icon>mdi-post</v-icon>
                    </v-list-item-icon>

                    <v-list-item-content>
                        <v-list-item-title>Uunzi community</v-list-item-title>
                    </v-list-item-content>
                </v-list-item>
                <v-list-item link href="/faqs">
                    <v-list-item-icon>
                        <v-icon>mdi-frequently-asked-questions</v-icon>
                    </v-list-item-icon>

                    <v-list-item-content>
                        <v-list-item-title>FAQ's</v-list-item-title>
                    </v-list-item-content>
                </v-list-item>
                <v-list-item link href="/about-us">
                    <v-list-item-icon>
                        <v-icon>mdi-information-variant</v-icon>
                    </v-list-item-icon>

                    <v-list-item-content>
                        <v-list-item-title>About</v-list-item-title>
                    </v-list-item-content>
                </v-list-item>

                {{-- <v-list-item link href="{{ route('loan.calculator') }}">
                <v-list-item-icon>
                    <v-icon>mdi-calculator</v-icon>
                </v-list-item-icon>

                <v-list-item-content>
                    <v-list-item-title>Loan Calculator</v-list-item-title>
                </v-list-item-content>
                </v-list-item> --}}

                {{-- <v-list-item link href="{{ route('add.company') }}">
                <v-list-item-icon>
                    <v-icon>mdi-plus</v-icon>
                </v-list-item-icon>

                <v-list-item-content>
                    <v-list-item-title>Add your listing</v-list-item-title>
                </v-list-item-content>
                </v-list-item> --}}

                @guest
                    <v-list-item link @click="auth_dialog = true">
                        <v-list-item-icon>
                            <v-icon>mdi-account-arrow-right</v-icon>
                        </v-list-item-icon>

                        <v-list-item-content>
                            <v-list-item-title>Join Us</v-list-item-title>
                        </v-list-item-content>
                    </v-list-item>
                @endguest
                @auth
                    <v-list-item link href="/{{ Auth::user()->isAdmin() ? 'uunzi-admin' : 'home' }}">
                        <v-list-item-icon>
                            <v-icon>mdi-home</v-icon>
                        </v-list-item-icon>

                        <v-list-item-content>
                            <v-list-item-title>Dashboard</v-list-item-title>
                        </v-list-item-content>
                    </v-list-item>



                    <v-list-item @click.prevent="logout()" link href="#">
                        <v-list-item-icon>
                            <v-icon>mdi-account-arrow-right</v-icon>
                        </v-list-item-icon>

                        <v-list-item-content>
                            <v-list-item-title>Sign Out</v-list-item-title>
                        </v-list-item-content>
                    </v-list-item>
                @endauth
            </v-list>
        </v-navigation-drawer>

        <v-app-bar color="#607d8b" dark app>
            <div class="hidden-md-and-up">
                <v-app-bar-nav-icon @click="drawer = !drawer"></v-app-bar-nav-icon>
            </div>
            <v-toolbar-title href="/">
                <a href="/">
                    <v-col align="end">
                        <v-img height="72px" width="72px" src="/images/icons/icon-384x384.png" id="logo_id"></v-img>
                    </v-col>
                </a>


            </v-toolbar-title>
            {{--<v-toolbar-title style="font-size: larger; font-family: 'Lobster', cursive;" class="text-capitalize"--}}
            {{--href="/">{{ config('app.name') }}</v-toolbar-title>--}}

            <v-spacer></v-spacer>
            <v-toolbar-items class="hidden-xs-only">

                <v-btn active class="text-capitalize" text href="/">Home</v-btn>
                <v-menu open-on-hover offset-y>
                    <template v-slot:activator="{ on }">
                        <v-btn class="text-capitalize" href="/category" text link v-on="on">
                            Categories
                        </v-btn>
                    </template>
                    <v-list>
                        @foreach (App\InnovatorSubcategory::all() as $category)
                            <v-list-item href="/category/{{ $category->name }}">
                                <v-list-item-title>{{ $category->name }}</v-list-item-title>
                            </v-list-item>
                        @endforeach
                    </v-list>
                </v-menu>
                <v-btn active class="text-capitalize" text href="/about-us">About Us</v-btn>
                <v-btn class="text-capitalize" text href="/faqs">FAQs</v-btn>
                <v-btn class="text-capitalize" text href="/uunzi-community">Uunzi community</v-btn>
                {{-- <v-btn class="text-capitalize" text href="{{ route('loan.calculator') }}">Loan Calculator
                </v-btn> --}}
                {{-- <v-btn class="text-capitalize" text href="{{ route('add.company') }}">Add your company</v-btn>
                --}}
                @guest
                    <main-auth :dialog="auth_dialog" v-on:updated="auth_dialog = $event"></main-auth>
                @endguest
                @auth
                    <v-menu offset-y>
                        <template v-slot:activator="{ on }">
                            <v-btn text class="text-capitalize" dark v-on="on">
                                {{ Auth::user()->name }}
                            </v-btn>
                        </template>
                        <v-list>
                            <v-list-item @click.prevent="logout()">
                                <v-list-item-avatar>
                                    <v-img src="https://www.gravatar.com/avatar/{{  Auth::user()->gravatar() }}">
                                    </v-img>
                                </v-list-item-avatar>
                                <v-list-item-title>Sign Out</v-list-item-title>
                            </v-list-item>
                            <v-list-item href="/{{ Auth::user()->isAdmin() ? 'uunzi-admin' : 'home' }}">
                                <v-list-item-title>Dashboard</v-list-item-title>
                            </v-list-item>
                            @if(Auth::user()->isAdmin())
                                <v-list-item href="/{{ Auth::user()->isAdmin() ? 'innovator-form-review':'/'}}">


                                    <v-list-item-content>
                                        <v-list-item-title>Add-Review</v-list-item-title>
                                    </v-list-item-content>
                                </v-list-item>
                            @endif

                        </v-list>
                    </v-menu>
                @endauth
            </v-toolbar-items>
            <!-- -->
        </v-app-bar>

        <!-- Sizes your content based upon application components -->
        <v-content>

            <!-- Provides the application the proper gutter -->


            <!-- If using vue-router -->
            @yield('content')

            <v-layout wrap justify-center class="mt-5 mb-5">
                <v-flex xs10 md4>
                    <news-letter id="newsLetter"></news-letter>
                </v-flex>
            </v-layout>
        </v-content>


        <v-layout justify-center style="background-color:#607D8B">
            <v-flex text-center xs12 md8>
                <v-btn target="blank" href="https://facebook.com/uunziofficial" dark icon>
                    <v-icon>mdi-facebook</v-icon>
                </v-btn>
                <v-btn target="blank" href="https://twitter.com/uunziofficial" dark icon>
                    <v-icon>mdi-twitter</v-icon>
                </v-btn>
                <v-btn dark target="blank" href="https://linkedin.com/company/uunziofficial" icon>
                    <v-icon>mdi-linkedin</v-icon>
                </v-btn>
                <v-btn dark target="blank" href="https://instagram.com/uunziofficial" icon>
                    <v-icon>mdi-instagram</v-icon>
                </v-btn>
            </v-flex>
        </v-layout>
        <v-footer color="#607D8B" padless justify-center>

            <v-flex text-center xs12>

                <v-btn href="/" color="white" text rounded class="my-2">
                    Home
                </v-btn>
                <v-btn href="/about-us" color="white" text rounded class="my-2">
                    About us
                </v-btn>
                <v-btn href="/faqs" color="white" text rounded class="my-2">
                    FAQ's
                </v-btn>

                <v-btn href="/uunzi-community" color="white" text rounded class="my-2">
                    Uunzi community
                </v-btn>

                <v-btn href="/register" color="white" text rounded class="my-2">
                    Join Us
                </v-btn>
                <v-flex color="#6B66D9C" white--text xs12>
                    <strong>{{ config('app.name') }}</strong> @{{ new Date().getFullYear() }}
                    <br>

                </v-flex>

                @yield('disclaimer')

            </v-flex>


        </v-footer>
    </v-app>
</div>
<script>
    window.onload = () => {
        @guest
        cookie.set('has_login', 0, {path: '/', expires: 1 / 24 / 60})
        @endguest
    }
</script>
<!--Start of Tawk.to Script-->
<script src="https://code.jquery.com/jquery-1.11.3.js"></script>

<script type="text/javascript">


    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5c693582f324050cfe339c0d/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
    @yield('script')

</script>
<!--End of Tawk.to Script-->
@yield('scripts')


</body>


</html>