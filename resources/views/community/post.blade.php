@extends('layouts.app')
@section('title', $post->title)
@section('description', $post->description)
@section('image', asset('storage/'.$post->image))
@section('content')

<section data-banner="">
    <v-card class="mx-auto" dark flat tile>
        <v-img class="white--text" height="300px" src="{{ asset('storage/'.$post->image) }}">
            <v-card-title style="text-shadow: 2px 2px 10px black"
                class="bold display-2 text-capitalize text-center align-center justify-center fill-height">
                {{ $post->title }}
            </v-card-title>
        </v-img>
        <share-button title="{{ $post->title }}" text="{{ $post->description }}" apiurl="{{ $post->slug }}" url="{{ config('app.url').'/'.$post->slug }}"></share-button>
    </v-card>
</section>
<section data-post-body="">
    <bread-crumbs :itemms="[
        {
            text: 'Uunzi',
            disabled: false,
            href: '/',
        },
        {
            text: 'Uunzi Community',
            disabled: false,
            href: '/uunzi-community'
        },
        {
            text: '{{ $post->title }}',
            disabled: true,
            href: '/{{ $post->slug }}'
        }]">
    </bread-crumbs>
    <v-container>
        <v-card flat>
            <span class="subtitle-1 font-weight-thin" data-published-date>Published on
                {{ date('D d M Y', strtotime($post->published_at)) }}</span>
            <div>
                @foreach ($post->tags as $tag)
                <v-chip color="primary" primary light disabled small>
                    {{ $tag->name }}
                </v-chip>
                @endforeach
            </div>
        </v-card>
        <v-layout pa-1 justify-center>
            <v-flex xs12 md6>
                {!! $post->body !!}
            </v-flex>
        </v-layout>
        <v-layout pa-1 justify-center>
        <v-flex xs12 md6 justify-center>
            <div id="disqus_thread"></div>
        </v-flex>
        </v-layout>
    </v-container>
</section>
@endsection
@section('scripts')
<script>

    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    var disqus_config = function () {
    this.page.url = "{{ url()->current() }}";  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = "{{ $post->slug }}"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://uunzi-com.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
    </script>

@endsection