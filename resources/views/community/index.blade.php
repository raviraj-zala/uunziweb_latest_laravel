@extends('layouts.app') @section('title', 'Uunzi community')
@section('description', 'Uuunzi community let\'s talk everything tech and innovation')
@section('content')
    <section data-banner-section="">
        <v-card flat tile>
            <v-img src="/images/uunzi-main-image-blue.png" max-height="450px">
                <v-layout column align-center class="white--text">
                    <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>
                    <h1 class="white--text mb-2 headingLora text-center">Uunzi Community</h1>
                    <div class="subheading ma-5 text-center">Articles and tips</div>
                </v-layout>


                <v-form method="GET" action="/search" id="searchForm" style="margin-bottom: 200px;">
                    <v-container grid-list-xl>
                        <v-layout justify-center align-content-center>
                            <v-flex xs12 sm12 md6>
                                <v-text-field height="60px" color="#00c6cf" name="query"
                                              label="What are you looking for?"
                                              rounded solo flat prefix="Search:" append-icon="mdi-magnify"
                                              @click:append="search()">
                            </v-flex>
                        </v-layout>
                    </v-container>
                    <input type="hidden" name="country" value="">
                </v-form>
            </v-img>
        </v-card>
    </section>

    <section data-posts-section>
        <bread-crumbs
                :itemms="[
    {
        text: 'Uunzi',
        disabled: false,
        href: '/',
    },
    {
        text: 'Uunzi Community',
        disabled: true,
        href: '/uunzi-community'
    }]">
        </bread-crumbs>
        <v-layout row px-10 ma-10>

            <div class="col-md-7 col-lg-7">
                @foreach ($posts as $post)
                    <v-flex class="pa-2">
                        <v-card href="{{ $post->slug }}" class="ma-3" hover flat tile>
                            <v-img src="{{ asset('storage/'.$post->image) }}" max-height="300"></v-img>
                            <div class="pa-2">
                                <div class="text-center">
                        <span
                                class="subtitle-1 font-weight-regular">{{ date('M d, Y', strtotime( $post->published_at)) }}
                            •
                            @foreach($post->tags as $tag) {{ $tag->name }} @endforeach </span>
                                </div>
                                <div>
                        <span
                                class="title font-weight-light font-weight-medium">{{ str_limit($post->title, 50, '...') }}</span>
                                </div>
                                <span class="body-1 font-weight-light">
                        {{ str_limit($post->description, 400, '') }}
                    </span>
                            </div>
                        </v-card>
                    </v-flex>

                @endforeach
            </div>
            <div class="col-md-2 col-lg-2">
                <v-flex>
                    <p>
                        {{--<span style="color: #f58634;text-shadow: 0px 0px 10px rgb(19, 18, 18)">  Are you looking for a technology solution?Yes, you are at right place.</span>--}}
                    </p>


                </v-flex>
            </div>

            <div class="col-md-3 col-lg-3">
                <v-flex>
                    <p>
                        <span style="color:#5cc6d0; font-size:20px"><b>Sign up to Uunzi community to remain current on technology news</b></span>
                    </p>

                    <v-card>
                        <v-layout align-center justify-center>
                            <v-card class="pa-4" id="signup-popup">
                                <communnity-component></communnity-component>
                            </v-card>
                        </v-layout>


                    </v-card>

                </v-flex>
            </div>

        </v-layout>

        {{ $posts->links() }}
    </section>


@endsection

@section('script')



        $(document).ready(function () {
            // Hide the div
            $("#signup-popup").hide();
            // Show the div after 5s
            $("#signup-popup").delay(5000).slideDown(500);
        });





@endsection

<script>
    import CommunnityComponent from "../../js/components/auth/CommunnityComponent";

    export default {
        components: {CommunnityComponent}
    }
</script>