@extends('layouts.app')

@section('content')
    <v-container grid-list-md fluid>
        <v-layout align-center justify-center row fill-height>
            <v-flex md6>
                <v-card flat class="pa-md-10">
                    <v-img src="images/banner_.png"></v-img>
                    <v-card-title>
                    <span style="color: #f58634; font-size: 24px;padding-left: 130px"> {{ __('Verify your email address


                    ') }}

                        </span>
                        <br>
                        {{ __('Please click on the link that has been sent to your email account') }}
                        </span>
                        <span>

                        {{ __('to verify your email and complete the registration process.') }}
                        </span>
                    </v-card-title>
                    @if (session('resent'))

                        <v-alert type="success" :value="true">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </v-alert>

                    @endif
                    <v-card-text style="padding-left: 150px">

                        <v-btn active rounded light dark color="green" class="text-capitalize ma-10 md-12 "
                               href="{{ route('verification.resend') }}" hover>
                            Verify Email
                        </v-btn>
                        {{--<a href="{{ route('verification.resend') }}" class="btn btn-info">>{{ __('click here to request another') }}</a>.--}}
                    </v-card-text>

                </v-card>
            </v-flex>
        </v-layout>
    </v-container>
@endsection