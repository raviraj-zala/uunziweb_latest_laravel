@extends('layouts.app')
@section('title', 'About Uunzi')
@section('description', 'About us -- Uunzi')
@section('content')
    <section data-banner-section="">
        <v-card flat tile>
            <v-img src="/images/uunzi-main-image-blue.png" max-height="350px">
                <v-layout column align-center class="white--text">
                    <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>
                    <h1 class="white--text mb-2 headingLora text-center">About Us</h1>
                    <bread-crumbs :itemms="[
                    {
                        text: 'Uunzi',
                        disabled: false,
                        href: '/',
                    },
                    {
                        text: 'About Us',
                        disabled: true,
                        href: '/about-us'
                    }]">
                    </bread-crumbs>
                </v-layout>
            </v-img>
        </v-card>
    </section>

    <section>
        <v-layout justify-center pa-2>
            <v-flex xs12 md7>
                <br>
                <h1 style="color:#ff8236;" class="mb-2 headingLora text-start">What is Uunzi?</h1>
                <div class="text-justify ma-5 pa-5 body-1">
                    Uunzi is a location-based platform that connects people to disruptive technology/ innovation
                    solutions local to them
                </div>
                <div>
                    <video style="width: 100%" height="50%" controls>
                        <source src="/images/Uunzi-About.mp4" type="video/mp4">
                        Your browser does not support HTML5 video.
                    </video>
                </div>

            </v-flex>
        </v-layout>
        <br>

        <v-layout justify-center pa-2>
            <v-flex xs12 md7>
                <h1 style="color:#ff8236;" class="mb-2 headingLora text-start">Why Uunzi?</h1>
                <div class="text-justify ma-5 pa-5 body-1">
                    I developed Uunzi because there is no marketplace that enables anyone looking for an app,
                    technology or solution to find it without having to plough on Google or waiting till you hear
                    from your friends and family about a cool app that has just been realised. Technology is at
                    the forefront of all development and is at the heart of progress of our generation, With Uunzi,
                    you can get updates on new cool tech applications based on your location, all at the touch of
                    your fingertips, you are connected to the most innovative tech solutions! Uunzi is currently
                    being piloted in Kenya but will expand to other territories in due course. If you are a budding
                    entrepreneur, interested in tech or just want to support Kenyan development then sign up to
                    our <a href="#newsLetter">newsletter</a>
                    <br>
                    <br>
                    <v-row align="center" justify="center">
                        <v-img width="100%" max-height="400" src="/images/ruth-wanjiku.jpg"></v-img>
                    </v-row>
                </div>
            </v-flex>
        </v-layout>

        <v-layout justify-center pa-2>
            <v-flex xs12 md7>
                <h1 style="color:#ff8236;" class="mb-2 headingLora text-start">Our Mission</h1>
                <div class="text-justify ma-5 pa-5 body-1">
                    <p>Nobody should be left behind by technology – Technology is for everyone – Technology has no
                        boundaries and Kenya is leading the way forward in East Africa. We are thinking locally,
                        expanding globally!
                        <br>
                        <br>
                        Our mission is to connect all people with innovative transformative technology. We place value in
                        ensuring all people irrespective of where they live, can have access to information regarding
                        technology and innovations that are local to them.
                        <br>
                        <br>
                        Our Promise: To remain curious, engaged, and innovative, adapting and evolving whilst putting
                        you the consumer and our clients first. We promise to listen to what you tell us and will give due
                        consideration to all feedback.
                    </p>
                </div>
            </v-flex>
        </v-layout>
    </section>
@endsection