@extends('layouts.app')
@section('title', 'Search')
@section('description', 'Search for solutions on uunzi')
@section('content')
<v-container grid-list-md fluid>
    <search-component query="{{ $query }}" country="{{ $country }}"></search-component>
</v-container>
@endsection