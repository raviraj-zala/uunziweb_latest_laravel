@extends('layouts.app')
@section('title', 'Add your company')
@section('description', 'Use this form to submit your company to uunzi today')
@section('content')
<v-container grid-list-md fluid>
  <v-layout align-center justify-center row fill-height>
    <v-flex md6>
      <v-card flat class="pa-md-6">
        <v-card-text>
          <h3 d-6>Fill your information and we will do the rest</h3>
          <add-company csrf={{ csrf_token() }} class="mt-6"></add-company>
        </v-card-text>
      </v-card>
    </v-flex>
  </v-layout>
</v-container>
@endsection