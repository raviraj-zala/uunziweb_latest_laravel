@extends('layouts.app')
@section('title', 'Sub-Categories')
@section('description', 'Uunzi Sub-Categories')
@section('content')
    <section data-banner-section="">
        <v-card flat tile>
            <v-img src="/images/uunzi-main-image-blue.png" max-height="350px">
                <v-layout column align-center class="white--text">
                    <v-img src="/images/big-logo-bold.png" alt="logo" title="logo" height="200" contain></v-img>
                    <h1 class="white--text mb-2 headingLora text-center">Sub-Categories</h1>
                    <bread-crumbs :itemms="[
                            {
                                text: 'Uunzi',
                                disabled: false,
                                href: '/',
                            },
                            {
                                text: 'Sub-Categories',
                                disabled: true,
                                href: '/category',
                            },
                           ]">
                    </bread-crumbs>
                </v-layout>
            </v-img>
        </v-card>
    </section>

    <br>
    <v-container grid-list-md fluid>
        <v-flex xs12>
            <h1 style="color:#ff8236;" class="mb-2 headingLora text-center"  href="/category">Categories</h1>
            <div style="color: #607d8b;font-size: 17px" class="subheading text-center">Search for the right company with Uunzi. With Uunzi, you can search, find and compare.</div>
            <br>

        </v-flex>

        <section data-innovators-new-section="">

            {{--<button id="button">Load</button>--}}
            <v-layout ma-5 pa-5 wrap align-center justify-center>

                @if (count($results) > 0)
                    @foreach ($results as $innovator)
                        <v-flex xs12 sm4 md6 lg3 ma-1 pa-0>
                            <v-card href="/category/{{ $innovator->name }}" hover height="360">
                                <center style="padding-top: 5px;">
                                  <span style="color:#607d8b"
                                        class="ml-1 headingLora">{{ str_limit($innovator->name, 30, '') }}</span>

                                </center>
                                <center style="padding-top: 10px;">

                                    <v-img height="180" width="250" src="/images/category/{{$innovator->name}}.png">
                                        <v-col align="end">

                                        </v-col>
                                    </v-img>
                                </center>

                                <v-list>
                                    <v-list-item>
                                        {{--<v-list-item-title>{{ $innovator->name }}</v-list-item-title>--}}
                                        {{--<br>--}}
                                        {{--<v-list-item-subtitle>--}}
                                        <p style="padding-left: 15px">
                                            Find available {{ $innovator->name }} opportunities with Uunzi. With Uunzi you can find the best {{ $innovator->name }} apps, start-ups and tech near you.
                                        </p>
                                        {{--</v-list-item-subtitle>--}}

                                    </v-list-item>
                                </v-list>

                            </v-card>
                        </v-flex>

                    @endforeach

                @endif
            </v-layout>

        </section>


        {{--<v-layout row wrap>--}}

        {{--@if (count($results) > 0)--}}
        {{--<v-flex xs12 sm3 md3 lg3>--}}
        {{--<section data-innovators-section="">--}}

        {{--</section>--}}
        {{--</v-flex>--}}
        {{--<v-flex xs12 sm6 md6 lg6 pa-10>--}}
        {{--<div class="row">--}}
        {{--<div class="col-sm-8"> <v-flex xs12 sm7 md7 lg7 wrap>--}}
        {{--@foreach ($results as $key=>$result)--}}
        {{--@if($key <= count($results)/2)--}}
        {{--<v-btn rounded light dark color="#607d8b" class="text-capitalize ma-1"--}}
        {{--href="/category/{{ $result->name }}" hover>--}}
        {{--<div style="color: #ffffff ">{{ $result->name }}</div>--}}
        {{--</v-btn>--}}
        {{--@endif--}}
        {{--@endforeach--}}
        {{--</v-flex></div>--}}
        {{--<div class="col-sm-4"> <v-flex xs12 sm10 md10 lg10 wrap>--}}
        {{--@foreach ($results as $key=>$result)--}}
        {{--@if($key > count($results)/2)--}}
        {{--<v-btn rounded light dark color="#607d8b" class="text-capitalize ma-1"--}}
        {{--href="/category/{{ $result->name }}" hover>--}}
        {{--<div style="color: #ffffff ">{{ $result->name }}</div>--}}
        {{--</v-btn>--}}
        {{--@endif--}}
        {{--@endforeach--}}
        {{--</v-flex>--}}
        {{--</div>--}}

        {{--</div>--}}






        {{--</v-flex>--}}
        {{--<v-flex xs12 sm3 md3 lg3 >--}}
        {{--<section data-innovators-section="">--}}

        {{--</section>--}}
        {{--</v-flex>--}}
        {{--@else--}}
        {{--<v-flex xs12 class="mb-2 headingLora text-center">--}}
        {{--<h4 style="color: #607d8b;">No categories are available yet!</h4>--}}
        {{--</v-flex>--}}
        {{--@endif--}}

        {{--</v-layout>--}}
    </v-container>

@endsection


