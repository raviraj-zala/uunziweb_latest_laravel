<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InnovatorSubcategory extends Model
{
    //mass assignable attributes
    protected $fillable = [
        'innovator_category_id',
        'name',
    ];

    /**
     * Get all the innovators in the current category
     * 
     */
    public function innovators()
    {
        return $this->hasMany('App\Innovator');
    }
}
