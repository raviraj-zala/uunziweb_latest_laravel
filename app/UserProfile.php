<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = [
        'user_id',
        'user_bio',
        'phone',
        'date_of_birth',
        'occupation'
    ];
}
