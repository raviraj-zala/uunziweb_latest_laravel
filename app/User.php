<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'country',
        'profile_picture',
        'role',
        'last_activity',
        'referal_code',
        'referer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role', 'last_activity', 'created_at', 'updated_at', 'email_verified_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_activity' => 'datetime'
    ];

    /**
     * get the current user's reviews
     * 
     * A user can make many reviews
     */
    public function reviews()
    {
        return $this->hasMany('App\InnovatorReview');
    }

    /**
     * A user can be an innovator or own companies
     * 
     * this method will return the innovation associated with the current user
     */
    public function company()
    {
        return $this->hasMany('App\Innovator');
    }


    /**
     * We need to check if the current user has an innovation
     * 
     * 
     */
    public function isInnovator()
    {
        return $this->company() == null ? false : true;
    }

    /**
     * A user has one profile
     * 
     */
    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }

    /**
     * Get the role of the current user
     * 
     * 
     */
    public function role()
    {
        return $this->role;
    }


    /**
     * Find out if the curent user is admin
     * 
     * 
     */
    public function isAdmin()
    {
        return $this->role === "admin";
    }

    /**
     * Check if the current user is staff
     * 
     */
    public function isStaff()
    {
        return $this->role === "staff";
    }

    /**
     * Get when the user was last active
     * 
     */
    public function lastSeen()
    {
        return $this->last_activity;
    }

    /**
     * Get the users gravatar
     * 
     * 
     */
    public function gravatar()
    {
        return md5($this->email);
    }
}
