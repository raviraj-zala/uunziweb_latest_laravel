<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //mass assignable atrributes
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    /**
     * A tag has many posts
     * 
     * 
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

    protected $hidden = ['pivot'];
}
