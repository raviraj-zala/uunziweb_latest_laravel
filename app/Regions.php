<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
    protected $fillable = [
        'name',
        'country'
    ];

    /**
     * No timestamps
     */
    protected $timestamps = false;
}
