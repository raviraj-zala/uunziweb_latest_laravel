<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Innovator extends Model
{
    use SoftDeletes, Notifiable;
    //mass assignable attributes
    protected $fillable = [
        'company_name',
        'slug',
        'company_tagline',
        'company_bio',
        'product_information',
        'additional_information',
        'cost',
        'benefits',
        'country',
        'sign_up_information',
        'contact_information',
        'company_email',
        'website_url',
        'logo',
        'inno_picture',
        'user_id',
        'innovator_category_id',
        'innovator_subcategory_id',
        'verified'
    ];

    /**
     * An innovation can belong to a user
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the category
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function category()
    {
        return $this->belongsTo('App\InnovatorCategory', 'innovator_category_id');
    }

    /**
     * Get the subcategory
     * 
     * belongs to one subcategory
     */
    public function subCategory()
    {
        return $this->belongsTo('App\InnovatorSubcategory', 'innovator_subcategory_id');
    }
    public function subCategoryMany()
    {
        return $this->belongsToMany('App\InnovatorSubcategory');
    }

    /**
     * An innovator has many reviews
     * 
     * 
     */
    public function reviews()
    {
        return $this->hasMany('App\InnovatorReview')->latest();
    }
    public function rating()
    {
        return $this->hasOne('App\GooglePlayReview');
    }

    /**
     * Check if the current innovator is verified
     * 
     * 
     */
    public function isVerified()
    {
        return $this->verified == 1;
    }

    /**
     * Route key name
     * 
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Find the average rating
     * 
     */
    public function averageRating()
    {
        $rating = number_format($this->reviews->avg('rating'),1);
       return $rating < 1 ? 0.0 : $rating;
    }


    /**
     * get the total number of ratings
     * 
     */
    public function totalReviews()
    {
        $number = count($this->reviews);
        return $number > 0 ? $number : 0 ;
    }

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->company_email;
    }

    protected $with = ['reviews'];
}
