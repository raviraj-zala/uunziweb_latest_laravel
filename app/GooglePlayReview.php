<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GooglePlayReview extends Model
{
    protected $table='googleplay_review';
}
