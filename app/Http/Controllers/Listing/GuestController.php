<?php

namespace App\Http\Controllers\Listing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuestController extends Controller
{
    /**
     * Submited a listing request will be processed here
     * 
     */
    public function handle(Request $request)
    {
        return $request;
    }
}
