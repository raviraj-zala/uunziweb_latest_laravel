<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Innovator;
use App\InnovatorSubcategory;

class SearchController extends Controller
{
    /**
     * Handle the incoming request
     * 
     */
    public function handle(Request $request)
    {
        $query = $request->input('query');
        $country = $request->input('country');
        return view('search.index', ['query' => $query, 'country' => $country]);
    }

    /**
     * This function is used to query the database
     * sort results filter them etc
     * 
     */
    public function search(Request $request)
    {
        
        if(!$request->isXmlHttpRequest()) {
            return abort(403, 'You don\'t have permission to visit this page');
        }
        $query = $request->input('query');
        $country = $request->input('country');
        $innovators = Innovator::where([
            ['company_name', 'like', '%'.$query.'%'],
            ['verified', 1],
            ['country', $country]
        ])->orWhere([
            ['product_information', 'like', '%'.$query.'%'],
            ['verified', 1],
            ['country', $country]
        ])->orWhere([
            ['additional_information', 'like', '%'.$query.'%'],
            ['verified', 1],
            ['country', $country],
        ])->with('reviews')->get(['id', 'company_name', 'logo', 'company_tagline', 'slug'])->take(50);

       // Innovator::with('reviews')->get(['id', 'company_name', 'logo', 'company_tagline'])->take(5);
        return $innovators;
    }

    /**
     * Filter results by category
     * @param [Query string]
     * @param [category]
     */
    public function filter(Request $request)
    {
        $query = $request->q;
        $categories = $request->categories;
        $results = [];

        foreach ($categories as $category) {
            $cat = InnovatorSubcategory::where('name', $category)->first();
            $inn = $cat->innovators;
            foreach ($inn as $value) {
                array_push($results, $value);
            }
        }

        return $results;
    }
}
