<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;


use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Newsletter;


class SocialLogin extends Controller
{
    /**
     * Let us handle github
     *
     *
     */
    public function githubLogin()
    {
        return Socialite::driver('github')
            ->scopes(['read:user', 'public_repo'])
            ->redirect();
    }

    /**
     * Let us handle github call back
     *
     *
     */
    public function githubCallback()
    {
        $user = Socialite::driver('github')->user();

        $checkIfUserExists = User::where('email', $user->email)->first();

        if ($checkIfUserExists !== null) {
            Auth::login($checkIfUserExists);
            return redirect('/');
        } elseif ($user->email == null) {
            return redirect('/login')->with('error', 'we did not find and email address');
        } else {
            $newUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => Hash::make($user->token),
                'active' => 1
            ]);

            if (!Newsletter::isSubscribed($user->email)) {
                Newsletter::subscribe($user->email);
            }
            Auth::login($newUser);

            return redirect('/');
        }
    }

    /**
     * Let us handle github
     *
     *
     */
    public function twitterLogin()
    {
        return Socialite::driver('twitter')
            ->redirect();
    }

    /**
     * Twitter callback
     *
     *
     *
     */
    public function twitterCallback()
    {
        $user = Socialite::driver('twitter')->user();

        $checkIfUserExists = User::where('email', $user->email)->first();

        if ($checkIfUserExists !== null) {
            Auth::login($checkIfUserExists);
            return redirect('/');
        } elseif ($user->email == null) {
            return redirect('/login')->with('error', 'we did not find and email address');
        } else {
            $newUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => Hash::make($user->token),
                'active' => 1
            ]);

            if (!Newsletter::isSubscribed($user->email)) {
                Newsletter::subscribe($user->email);
            }
            Auth::login($newUser);

            return redirect('/');
        }
    }

    /**
     * Let us handle facebook
     *
     *
     */
    public function facebookLogin()
    {
        return Socialite::driver('facebook')
            ->redirect();
    }

    /**
     * Facebook callback
     *
     *
     */
    public function facebookCallback()
    {


        $user = Socialite::driver('facebook')->user();
        return $user;
        $checkIfUserExists = User::where('email', $user->email)->first();

        if ($checkIfUserExists !== null) {
            Auth::login($checkIfUserExists);
            return redirect('/');
        } elseif ($user->email == null) {
            return redirect('/login')->with('error', 'we did not find and email address');
        } else {
            $newUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => Hash::make($user->token),
                'active' => 1
            ]);

            if (!Newsletter::isSubscribed($user->email)) {
                Newsletter::subscribe($user->email);
            }

            Auth::login($newUser);

            return redirect('/');
        }
    }

    /**
     * Let us handle google
     *
     */
    public function googleLogin()
    {
        return Socialite::driver('google')
            ->redirect();
    }

    /**
     * Google callback
     *
     */
    public function googleCallback()
    {
        $user = Socialite::driver('google')->user();

        $checkIfUserExists = User::where('email', $user->email)->first();

        if ($checkIfUserExists !== null) {
            Auth::login($checkIfUserExists);
            return redirect('/');
        } elseif ($user->email == null) {
            return redirect('/login')->with('error', 'we did not find and email address');
        } else {
            $newUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => Hash::make($user->token),
                'active' => 1
            ]);

            if (!Newsletter::isSubscribed($user->email)) {
                Newsletter::subscribe($user->email);
            }

            Auth::login($newUser);

            return redirect('/');
        }
    }
}
