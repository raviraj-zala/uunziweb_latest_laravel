<?php

namespace App\Http\Controllers;

use App\GooglePlayReview;
use App\Innovator;
use App\InnovatorCategory;
use App\InnovatorSubcategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Raulr\GooglePlayScraper\Scraper;

class InnovatorSubcategoryController extends Controller
{

    public function testReview(Request $request)
    {
        return $request;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('dd');
        $results = InnovatorSubcategory::all();
//
        return view('subcategories.index', ['results' => $results]);
    }

    public function indexget()
    {
        
        return $results = InnovatorSubcategory::all();
//
//        return view('subcategories.index', ['results' => $results]);
    }

    public function indexdata(Request $request)
    {

        $countcheck = InnovatorCategory::all();
        if (count($countcheck) > 1) {
            $names = implode(',', $request->name);
            $explodingName = explode(',', $names);
            $latestName = $explodingName[count($explodingName) - 1];
            $id = InnovatorCategory::where('name', $latestName)->pluck('id')->first();
            return $results = InnovatorCategory::where('main_category', $id)->get();

        } else {

            $name = $request->name[0];
            $id = InnovatorCategory::where('name', $name)->pluck('id')->first();
            return $results = InnovatorCategory::where('main_category', $id)->get();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = [
            'innovator_category_id' => 'required',
            'name' => 'required',
        ];

        InnovatorSubcategory::create($request->all());

        return response('created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InnovatorSubcategory $innovatorSubcategory
     * @return \Illuminate\Http\Response
     */
    public function show($innovatorSubcategory)
    {

//        return Carbon::now()->subDays(90);

        $currentCategory = InnovatorSubcategory::where('name', $innovatorSubcategory)->first();

        if (!$currentCategory) {
            return abort(404);
        }
//        $query->where('country', geoip()->getLocation()->country)
        $results = $currentCategory->load(['innovators' => function ($query) {
            $query->where('verified', 1);
        }]);


        return view('categories.index',
            ['results' => $results,]);
    }

    public function showIndex_Min_Max_Loan_Amount(Request $request)
    {

        $currentCategory = InnovatorSubcategory::where('name', $request->category)->first();

        if (!$currentCategory) {
            return abort(404);
        }

        $innovators = Innovator::where('innovator_subcategory_id', $currentCategory->id)->with('rating')
//            $query->where('country', geoip()->getLocation()->country)
            ->when($request->loan_value, function ($q) use ($request) {
                return $q->where('minimum_loan_value', '<', $request->loan_value)
                    ->where('maximum_loan_value', '>', $request->loan_value)
                    ->orWhere('minimum_loan_value', $request->loan_value)
                    ->orWhere('maximum_loan_value', $request->loan_value);
            })
            ->when($request->repayment_time, function ($q) use ($request) {
                return $q->where('repayment_period_max', '>', $request->repayment_time)
                    ->where('repayment_period_min', '<', $request->repayment_time);

            })
            ->when($request->interest_rate_value, function ($q) use ($request) {

                return $q->where('interest_rate_mode', $request->interest_type)
                    ->where('interest_rate_min', '<', $request->interest_rate_value)
                    ->where('interest_rate_max', '>', $request->interest_rate_value)
                    ->orWhere('interest_rate_max', $request->interest_rate_value)
                    ->orWhere('interest_rate_min', $request->interest_rate_value)
                    ->where('interest_rate_mode', $request->interest_type);

            })
            ->where('verified', 1)->get();

        if ($innovators->count() <= 0) {
            return '<span class="headingLora"><br>Sorry, we couldn’t found any company that matches your specification, please search
        again with different specification</span>';
        }
        $outputs = '';


        foreach ($innovators as $innovator) {
            $stars = '';
            $rating = '';
            $votes = '';
            if ($innovator->rating != null) {
                if ($innovator->rating->rating == 0.0 && $innovator->rating->rating < 1.0) {
                    $stars = '<span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
                } elseif ($innovator->rating->rating >= 1.0 && $innovator->rating->rating < 2.0) {
                    $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
                } elseif ($innovator->rating->rating >= 2.0 && $innovator->rating->rating < 3.0) {
                    $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
                } elseif ($innovator->rating->rating >= 3.0 && $innovator->rating->rating < 4.0) {
                    $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
                } elseif ($innovator->rating->rating >= 4.0 && $innovator->rating->rating < 5.0) {
                    $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star unchecked"></span>';
                } elseif ($innovator->rating->rating == 5.0 && $innovator->rating->rating < 6.0) {
                    $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>';
                }
                $rating = ($innovator->rating->rating) . '%';
                $votes = "(" . $innovator->rating->votes . ")";

            }

            $outputs .= '<div id="products" class="row view-group ma-2 myBox"  flat hover>
                            <a href="/innovators/' . $innovator->slug . '"></a>
                            <div class="item col-xs-3 col-sm-4 col-md-6 col-lg-12">
                                <div class="thumbnail card">
                          
                                    <div class="caption card-body">
                                        <h4 class="group card-title inner list-group-item-heading">
                                        </h4>
                                        <div style="display: inline-flex;">
                                            <image height="120" width="125" src="/storage/' . $innovator->logo . '">
                                            </image>
                                            <p class="group inner list-group-item-text" style="padding-left: 10px">
                                            <span style="color: #a8cf45;"
                                                  class="headingLora">' . str_limit($innovator->company_name, 23, " ") . '</span>
                                                <br>
                                                <br>
                                                <span style="color: #f58634;">' . $innovator->company_tagline . '</span>
                                                <br>
                                                <br>
                                               ' . $innovator->company_bio . '
                                            </p>
                                        </div>
                                        <div class="row">
                                        
                                            <div class="col-xs-12 col-md-12 row ma-2">
                                            <div><h1 style="color: #606060">' . $rating . '</h1></div>
                                                ' . $stars . '
                                            <div>' . $votes . '</div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
        }

        return $outputs;
    }


    public
    function showIndex_Review(Request $request)
    {
        $currentCategory = InnovatorSubcategory::where('name', $request->category)->first();

        if (!$currentCategory) {
            return abort(404);
        }


        $results = $currentCategory->load(['innovators' => function ($query) use ($request) {
            $query->where('country', geoip()->getLocation()->country)
                ->where('verified', 1);
        }]);

        if ($results->innovators->count() <= 0) {
            return '<h1><br>We could not find any tech solutions of  ' . $results->name . ' between  this review range. Please check back
                        later</h1>';
        }
        $outputs = '';
        $stars = '';
        foreach ($results->innovators as $innovator) {

            if ($innovator->rating->rating == 0.0 && $innovator->rating->rating < 1.0) {
                $stars = '<span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
            } elseif ($innovator->rating->rating >= 1.0 && $innovator->rating->rating < 2.0) {
                $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
            } elseif ($innovator->rating->rating >= 2.0 && $innovator->rating->rating < 3.0) {
                $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
            } elseif ($innovator->rating->rating >= 3.0 && $innovator->rating->rating < 4.0) {
                $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star unchecked"></span>
                            <span class="fa fa-star unchecked"></span>';
            } elseif ($innovator->rating->rating >= 4.0 && $innovator->rating->rating < 5.0) {
                $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star unchecked"></span>';
            } elseif ($innovator->rating->rating == 5.0 && $innovator->rating->rating < 6.0) {
                $stars = '<span class="fa fa-star checked" ></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>';
            }
            if ($request->review_id == "all") {
                $outputs .= '<div id="products" class="row view-group ma-2 myBox"  flat hover>
                            <a href="/innovators/' . $innovator->slug . '"></a>
                            <div class="item col-xs-3 col-sm-4 col-md-6 col-lg-12">
                                <div class="thumbnail card">
                          
                                    <div class="caption card-body">
                                        <h4 class="group card-title inner list-group-item-heading">
                                        </h4>
                                        <div style="display: inline-flex;">
                                            <image height="120" width="125" src="/storage/' . $innovator->logo . '">
                                            </image>
                                            <p class="group inner list-group-item-text" style="padding-left: 10px">
                                            <span style="color: #a8cf45; "
                                                  class="hh">' . str_limit($innovator->company_name, 23, " ") . '</span>
                                                <br>
                                                <br>
                                                <span style="color: #f58634;">' . $innovator->company_tagline . '</span>
                                                <br>
                                                <br>
                                               ' . $innovator->company_bio . '
                                            </p>
                                        </div>
                                        <div class="row">

                                            <div class="col-xs-12 col-md-12 row ma-2">

                                                <div>' . $innovator->rating->rating . '%</div>
                                                
                                       ' . $stars . '
                                                
                                                <div>(' . $innovator->rating->votes . ')</div>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';

            } else {
                if ($innovator->averageRating() == $request->review_id) {
                    $outputs .= '<div id="products" class="row view-group ma-2 myBox"  flat hover>
                            <a href="/innovators/' . $innovator->slug . '"></a>
                            <div class="item col-xs-3 col-sm-4 col-md-6 col-lg-12">
                                <div class="thumbnail card">
                          
                                    <div class="caption card-body">
                                        <h4 class="group card-title inner list-group-item-heading">
                                        </h4>
                                        <div style="display: inline-flex;">
                                            <image height="120" width="125" src="/storage/' . $innovator->logo . '">
                                            </image>
                                            <p class="group inner list-group-item-text" style="padding-left: 10px">
                                            <span style="color: #a8cf45; "
                                                  class="ml-1 title">' . str_limit($innovator->company_name, 23, " ") . '</span>
                                                <br>
                                                <br>
                                                <span style="color: #f58634;">' . $innovator->company_tagline . '</span>
                                                <br>
                                                <br>
                                               ' . $innovator->company_bio . '
                                            </p>
                                        </div>
                                          <div class="row">

                                            <div class="col-xs-12 col-md-12 row ma-2">

                                                <div>' . $innovator->rating->rating . '%</div>
                                                
                                       ' . $stars . '
                                                
                                                <div>' . $innovator->rating->votes . '</div>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
                }
            }
        }
        return $outputs;
    }


    public
    function FilterDataLoan(Request $request)
    {
        return $request;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InnovatorSubcategory $innovatorSubcategory
     * @return \Illuminate\Http\Response
     */
    public
    function edit(InnovatorSubcategory $innovatorSubcategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\InnovatorSubcategory $innovatorSubcategory
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, InnovatorSubcategory $innovatorSubcategory)
    {
        $valid = [
            'innovator_category_id' => 'required',
            'name' => 'required',
        ];

        $innovatorSubcategory->update($request->all());

        return response('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InnovatorSubcategory $innovatorSubcategory
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(InnovatorSubcategory $innovatorSubcategory)
    {

        $innovatorSubcategory->delete();

        return response('deleted');
    }
}
