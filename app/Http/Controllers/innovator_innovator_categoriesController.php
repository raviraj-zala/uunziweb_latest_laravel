<?php

namespace App\Http\Controllers;

use App\innovator_innovator_category;
use Illuminate\Http\Request;

class innovator_innovator_categoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\innovator_innovator_category  $innovator_innovator_category
     * @return \Illuminate\Http\Response
     */
    public function show(innovator_innovator_category $innovator_innovator_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\innovator_innovator_category  $innovator_innovator_category
     * @return \Illuminate\Http\Response
     */
    public function edit(innovator_innovator_category $innovator_innovator_category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\innovator_innovator_category  $innovator_innovator_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, innovator_innovator_category $innovator_innovator_category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\innovator_innovator_category  $innovator_innovator_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(innovator_innovator_category $innovator_innovator_category)
    {
        //
    }
}
