<?php

namespace App\Http\Controllers\Community;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class CommunityController extends Controller
{
    /**
     * Handle the index request
     */
    public function index()
    {
        $posts =  Post::orderBy('published_at', 'desc')->paginate(12);
        return view('community.index', ['posts' => $posts]);
    }
}
