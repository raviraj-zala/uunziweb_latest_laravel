<?php

namespace App\Http\Controllers\NewsLetter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Newsletter;

class NewsLetterController extends Controller
{
    public function handle(Request $request)
    {
        if ( ! Newsletter::isSubscribed($request->email) ) {
            Newsletter::subscribe($request->email,  ['FNAME'=>$request->first_name, 'LNAME'=>$request->last_name]);
        }

        // return Newsletter::getMembers();
        return response('Thank you for joining our list');
    }
}
