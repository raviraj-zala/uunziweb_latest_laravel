<?php

namespace App\Http\Controllers;

use App\Innovator;
use App\InnovatorReview;
use function foo\func;
use Illuminate\Http\Request;

class InnovatorReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function addreview(Request $request)
    {



        $innovator = Innovator::where('company_name', $request->app_name)->first();

        $validate = InnovatorReview::where('innovator_id', $innovator->id)->first();
        if ($validate != null) {
            return "already";
        }

        for ($i = 1; $i <= $request->comment_count; $i++) {

            $reviews = new InnovatorReview();
            $reviews->user_id = 294;
            $reviews->innovator_id = $innovator->id;
            $reviews->comment = $request[$i];
            $reviews->rating = $request[$i."_r"];
            $reviews->save();
        }


    }

    public function addreviewpage()
    {
        return view('reviewadd');


    }

    public function getreviewapps()
    {
        $innovators = Innovator::all();
        $new_innovators = array();
        foreach ($innovators as $innovator) {

            if ($innovator->website_url != null) {
                $res = explode('=', $innovator->website_url);

                if (count($res) > 1) {
                    array_push($new_innovators, $innovator);
                }

            }
        }


        $arrayInnovator = array();
        foreach ($new_innovators as $innovator) {
            if (sizeof($innovator->reviews) == 0) {
                $arrayInnovator[] = $innovator;
            }
        }
        return $arrayInnovator;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request;
        $valid = [
            "user_id" => 'required',
            "innovator_id" => 'required',
            "comment" => 'required',
            "rating" => 'required'
        ];

        $user = $request->user()->id;
        $userId = ['user_id' => $user];
        $received = $request->all();
        $fullReview = array_merge($userId, $received);

        InnovatorReview::create($fullReview);

        return response('created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InnovatorReview $innovatorReview
     * @return \Illuminate\Http\Response
     */
    public function show(InnovatorReview $innovatorReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InnovatorReview $innovatorReview
     * @return \Illuminate\Http\Response
     */
    public function edit(InnovatorReview $innovatorReview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\InnovatorReview $innovatorReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InnovatorReview $innovatorReview)
    {
        $valid = [
            "user_id" => 'required',
            "innovator_id" => 'required',
            "comment" => 'required',
            "rating" => 'required'
        ];

        $user = $request->user()->id;
        $userId = ['user_id' => $user];
        $received = $request->all();
        $fullReview = array_merge($userId, $received);

        $innovatorReview->update($fullReview);

        return response('created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InnovatorReview $innovatorReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(InnovatorReview $innovatorReview)
    {
        $innovatorReview->delete();

        return response('deleted');
    }
}
