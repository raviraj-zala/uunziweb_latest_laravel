<?php

namespace App\Http\Controllers\LandingPage;

use App\GooglePlayReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Innovator;
use App\InnovatorSubcategory;
use App\Post;


use Illuminate\Support\Facades\Auth;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Access\AuthorizationException;

use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Raulr\GooglePlayScraper\Scraper;

class LandingPageController extends Controller
{
    //
    public function index(Request $request)
    {


//        $pathToImage = 'images\pexels-photo-1226302.jpeg';
//        $optimizerChain = OptimizerChainFactory::create();
//        $optimizerChain->optimize($pathToImage);


        $sitemap = Sitemap::create()
            ->add(Url::create('/uunzi-admin'))
            ->add(Url::create('/home'))
            ->add(Url::create('/about-us'))
            ->add(Url::create('/faqs'))
            ->add(Url::create('/uunzi-community'));

        InnovatorSubcategory::all()->each(function (InnovatorSubcategory $newsItem) use ($sitemap) {
            $sitemap->add(Url::create("/category/{$newsItem->name}"));
        });


        $sitemap->writeToFile(public_path('sitemap.xml'));


        if (Auth::user()) {

            if (Auth::user()->hasVerifiedEmail()) {

                $innovators = $this->trendingInnovators($request->ip());
                $posts = $this->latestArticles();
                $country = $this->getUserLocale();
                $categories = $this->categories();
                $latestInnovators = $this->latestInnovators();
                return view(
                    'welcome',
                    [
                        'innovators' => $innovators,
                        'posts' => $posts,
                        'country' => $country,
                        'categories' => $categories,
                        'latestInnovators' => $latestInnovators
                    ]
                );
            } else {
                return view('verifypage');
            }
        } else {
            $innovators = $this->trendingInnovators($request->ip());
            $posts = $this->latestArticles();
            $country = $this->getUserLocale();
            $categories = $this->categories();
            $latestInnovators = $this->latestInnovators();
            return view(
                'welcome',
                [
                    'innovators' => $innovators,
                    'posts' => $posts,
                    'country' => $country,
                    'categories' => $categories,
                    'latestInnovators' => $latestInnovators
                ]
            );
        }


    }

    /**
     * get the trending innovators
     *
     * it is location based
     */
    public function trendingInnovators($ip = null)
    {
        //for now let us get random
        $userLocation = $this->getUserLocale($ip);
        return Innovator::where('country', $userLocation)->inRandomOrder()->limit(4)->get();
    }

    /**
     * Get the latest innovators
     *
     */
    public function latestInnovators($ip = null)
    {
        $userLocation = $this->getUserLocale($ip);
        return Innovator::where('country', $userLocation)->latest()->limit(4)->get();
    }

    /**
     * Get user Locale
     *
     * content needs to be location based
     */
    public function getUserLocale($ip = null)
    {
        //for now let us return kenya
        // this would be used to return innovators
//        return geoip()->getLocation()->country;
        return 'Kenya';
    }

    /**
     * Get trending categories
     *
     */
    public function trendingCategories()
    {
        //leave this blank
    }

    /**
     * Get the top Three articles
     * or the latest three
     */
    public function latestArticles()
    {
        return Post::OrderBy('published_at', 'desc')->limit(4)->get();
    }


    /**
     * Prevent access to ckfinder
     *
     *
     */
    public function ckFinder()
    {
        return abort(404, 'Nice try Mr . ');
    }

    /**
     * Get categories
     *
     */
    public function categories()
    {
        return InnovatorSubcategory::inRandomOrder()->limit(5)->get();
    }
}
