<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Innovator;
use App\InnovatorImage;
use App\InnovatorReview;
use App\Post;
use App\User;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /**
     * Validate email
     * 
     * @return
     */
    public function validateEmail(Request $request)
    {
        $email = $request->email;
        $results =  DB::table('users')->select('email')->where('email', '=', $email)->get();
        if (count($results) > 0) {
            return ['message' => 'This email is already registered'];
        } else {
            return ['valid' => 'valid'];
        }
    }

    /**
     * Get all posts
     * 
     * @return App\Posts
     */
    public function getPosts()
    {
        return Post::with('tags')->get();
    }

    /**
     * Get a particular post
     * 
     * @return App\Post
     */
    public function getPost(Request $request, Post $post)
    {
        return $post;
    }

    /**
     * Get all Innovators
     * 
     * @return App\Innovator
     */
    public function getInnovators()
    {
        return Innovator::with('reviews')->get();
    }

    /**
     * get innovator
     * 
     * @param 
     * return a particular innovator
     */
    public function getInnovator(Request $request, Innovator $innovator)
    {
        return $innovator->load('reviews');
    }


    /**
     * Get user review
     * I was too lazy to create another controller so 
     * i put you here
     * 
     * @param User
     */
    public function getUserReview(Request $request, $innovator)
    {
        $user = $request->user()->id;
        return InnovatorReview::where(['innovator_id' => $innovator, 'user_id' => $user])->first();
    }
}
