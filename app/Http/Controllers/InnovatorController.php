<?php

namespace App\Http\Controllers;

use App\Innovator;
use App\innovator_innovator_category;
use App\InnovatorCategory;
use App\InnovatorSubcategory;
use App\InnovatorImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Log;

class InnovatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
        $valid = $request->validate([
            'company_name' => 'unique:innovators'
        ]);
        // $time = time();
        $path = $request->logo->storeAs('innovators', str_slug($request->company_name, '-') . '.' . $request->file('logo')->extension(), 'public');
        // $inno_picture_path = $request->inno_picture->storeAs('innovators', str_slug($request->company_name, '-') . '.' . $request->file('inno_picture')->extension(), 'public');
        $inno_video_path = $request->inno_video->storeAs('innovators', str_slug($request->company_name, '-') . '.' . $request->file('inno_video')->extension(), 'public');   
        // $inno_picture_path = $request->file('inno_picture');
        $inno = $request->inno_picture;
        // Log::debug ($inno);

        $innovator = new Innovator();
        $innovator->company_name = $request->company_name;
        $innovator->slug = str_slug($request->company_name);
        $innovator->company_tagline = $request->company_tagline;
        $innovator->company_bio = $request->company_bio;
        $innovator->product_information = $request->product_information;
        $innovator->additional_information = $request->additional_information;
        $innovator->cost = $request->cost;
        $innovator->benefits = $request->benefits;
        $innovator->country = $request->country;
        $innovator->sign_up_information = $request->sign_up_information;
        $innovator->contact_information = $request->contact_information;
        $innovator->company_email = $request->company_email;
        $innovator->website_url = $request->website_url;
        $innovator->logo = $path;
        // $innovator->inno_picture = $inno_picture_path;
        $innovator->inno_video = $inno_video_path;
        $innovator->user_id = null;
        $innovator->verified = 0;
        $innovator->minimum_loan_value = $request->minimumLoan;
        $innovator->maximum_loan_value = $request->maximumLoan;
        $innovator->repayment_period_min = $request->RepaymentPeriod_min;
        $innovator->repayment_period_max = $request->RepaymentPeriod_max;
        $innovator->interest_rate_min = $request->IntrestRateMin;
        $innovator->interest_rate_max = $request->IntrestRateMax;
        $innovator->interest_rate_mode = $request->IntrestRateMode;
        // $innovator->save();

        if($innovator->save()){

            $count = 0;
            foreach($request->file('inno_picture') as $image)
            {
                $count++;
                $time = time();
                $name = $count.$time . '.'. $image->getClientOriginalExtension();
                $image->move(storage_path().'/uunzi_new/admin/innovators_picture', $name);
                $image = $name;
                $innoImg = new InnovatorImage();
                $innoImg->innovator_id = $innovator->id;
                $innoImg->inno_picture = $image;
                $innoImg->save();
            }
        }    

        //        foreach (explode(',', $request->category) as $value) {
//          $temp=  str_replace('"', ' ', $value);
//            InnovatorCategory::firstOrCreate(
//                ['name' =>$temp]
//            );
//        }
//        foreach (explode(',', $request->subcategory) as $value2) {
//            $temp2=  str_replace('"', ' ', $value2);
//            InnovatorSubcategory::firstOrCreate(
//                ['name' => $temp2]
//            );
//        }
        
        $arrySubCat = explode(',', $request->subcategory);
        foreach ($arrySubCat as $value) {

            $name = str_replace('"', ' ', $value);
            $id = InnovatorCategory::where('name', $name)->pluck('id')->first();
            $piviot_sub_category = new innovator_innovator_category();
            $piviot_sub_category->innovator_id = $innovator->id;
            $piviot_sub_category->innovator_subcategory_id = $id;
            $piviot_sub_category->save();

        }

        return response('added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Innovator $innovator
     * @return \Illuminate\Http\Response
     */
    public function show(Innovator $innovator)
    {
//       return $innovator->load(['reviews', 'subCategory', 'category']);
        $similar_categories = Innovator::where('innovator_subcategory_id', $innovator->innovator_subcategory_id)
            ->where('id', '<>', $innovator->id)
//            ->where('country', geoip()->getLocation()->country)
            ->limit(5)->get();
        if (!$similar_categories) {
            return abort(404);
        }
        $country = geoip()->getLocation()->country;
        return view('innovator.view',
            [
                'innovator' => $innovator->load(['rating', 'subCategory', 'category']),
                'similar_categories' => $similar_categories,
                'country' => $country
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Innovator $innovator
     * @return \Illuminate\Http\Response
     */
    public function edit(Innovator $innovator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Innovator $innovator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Innovator $innovator)
    {
        $innovator->company_name = $request->company_name;
        $innovator->slug = str_slug($request->company_name);
        $innovator->company_tagline = $request->company_tagline;
        $innovator->company_bio = $request->company_bio;
        $innovator->product_information = $request->product_information;
        $innovator->additional_information = $request->additional_information;
        $innovator->cost = $request->cost;
        $innovator->benefits = $request->benefits;
        $innovator->country = $request->country;
        $innovator->sign_up_information = $request->sign_up_information;
        $innovator->contact_information = $request->contact_information;
        $innovator->company_email = $request->company_email;
        $innovator->website_url = $request->website_url;
        $innovator->user_id = null;


        if ($request->category !== null) {
            $innovator->category()->associate(
                InnovatorCategory::firstOrCreate(
                    ['name' => $request->category]
                )
            );
        }

        if ($request->subcategory !== null) {
            $innovator->subCategory()->associate(
                InnovatorSubcategory::firstOrCreate(
                    ['name' => $request->subcategory]
                )
            );
//            if($request)
            $innovator->minimum_loan_value = $request->minimumLoan;
            $innovator->maximum_loan_value = $request->maximumLoan;
            $innovator->repayment_period_min = $request->RepaymentPeriod_min;
            $innovator->repayment_period_max = $request->RepaymentPeriod_max;
            $innovator->interest_rate_min = $request->IntrestRateMin;
            $innovator->interest_rate_max = $request->IntrestRateMax;
            $innovator->interest_rate_mode = $request->IntrestRateMode;
        }

        $innovator->save();


        return response('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Innovator $innovator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Innovator $innovator)
    {

        $innovator->delete();

        return response('deleted');
    }

    /**
     * Get Simillar innovators
     *
     * for now we use categories and subcategories
     *
     */
    public function getSimmilar(Innovator $innovator)
    {
        $category = $innovator->subCategory;
        return $category->innovators;
    }
}
