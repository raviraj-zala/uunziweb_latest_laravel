<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Post::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = [
            'title' => 'required|unique:posts',
            'slug' => 'required',
            'author' => 'required',
            'body' => 'required',
            'published_at', 'required'
        ];


        $path = $request->image->storeAs('posts', str_slug($request->title, '-') . '.' . $request->image->extension(), 'public');

        $slug = str_slug($request->title);
        $post = new Post();
        $post->image = $path;
        $post->slug = $slug;
        $post->title = $request->title;
        $post->author = $request->author;
        $post->body = $request->body;
        $post->description = $request->description;
        $post->published_at = $request->published_at;
        $post->save();

        if ($request->tags) {
            foreach (explode(',', $request->tags) as $value) {
                $post->tags()->save(
                    Tag::firstOrCreate(['name' => $value])
                );
            }
        }

        return response('created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

        return view('community.post', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $valid = [
            'title' => 'required',
            'slug' => 'required',
            'author' => 'required',
            'body' => 'required',
            'published_at', 'required'
        ];

        $slug = str_slug($request->title);
        $post->slug = $slug;
        $post->title = $request->title;
        $post->author = $request->author;
        $post->body = $request->body;
        $post->description = $request->description;
        $post->published_at = $request->published_at;
        $post->save();


        return response('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return response('deleted');
    }
}
