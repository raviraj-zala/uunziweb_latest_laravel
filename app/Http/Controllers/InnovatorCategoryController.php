<?php

namespace App\Http\Controllers;

use App\Innovator;
use App\InnovatorCategory;
use Illuminate\Http\Request;

class InnovatorCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return $results = InnovatorCategory::where('main_category',0)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkAlready = InnovatorCategory::where('name', $request->category_name)->first();
        if ($checkAlready) {
            return "already";
        }

        $valid = $request->validate([
            'category_name' => 'required'
        ]);

        $category = new InnovatorCategory();
        $category->name = $request->category_name;
        $category->save();
        return response('updated');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InnovatorCategory $innovatorCategory
     * @return \Illuminate\Http\Response
     */
    public function show(InnovatorCategory $innovatorCategory)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InnovatorCategory $innovatorCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(InnovatorCategory $innovatorCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\InnovatorCategory $innovatorCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InnovatorCategory $innovatorCategory)
    {
        $valid = $request->validate([
            'name' => 'required'
        ]);

        $innovatorCategory->update($request->all());

        return response('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InnovatorCategory $innovatorCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(InnovatorCategory $innovatorCategory)
    {

        $innovatorCategory->delete();

        return response('deleted');
    }
}
