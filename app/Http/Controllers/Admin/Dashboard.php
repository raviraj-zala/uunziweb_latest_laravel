<?php

namespace App\Http\Controllers\Admin;

use App\InnovatorCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Innovator;
use App\Mail\ContactUser;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Mail;

class Dashboard extends Controller
{
    /**
     * Handle all requests
     *
     * The roo
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * Handle the users route
     *
     */
    public function users()
    {
        return view('admin.users');
    }

    /**
     * Handle the posts route
     *
     */
    public function posts()
    {

        return view('admin.posts');
    }

    /**
     * Handle innovators route
     *
     */
    public function innovators()
    {
        return view('admin.innovators');
    }

    public function categories()
    {
        return view('admin.categories');
    }

    /**
     * get dashboard data
     *
     */
    public function getData(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return abort(403, 'You cannot be here');
        }
        $users = User::count();
        $posts = Post::count();
        $innovators = Innovator::count();

        return ['users' => $users, 'posts' => $posts, 'innovators' => $innovators];
    }

    /**
     * Get innovators
     *
     * @return JSON
     */
    public function getInnovators()
    {
        return Innovator::with(['category', 'subCategory'])->latest()->get();
    }

    public function getCategories()
    {
        return InnovatorCategory::where('main_category',0)->latest()->get();
    }

    /**
     * Handle innovator verification
     *
     * @method
     */
    public function handleInnovatorVerification(Innovator $innovator)
    {
        if ($innovator->isVerified()) {
            $innovator->verified = 0;
            $innovator->save();
            return 0;
        } else {
            $innovator->verified = 1;
            $innovator->save();
            return 1;
        }
    }

    /**
     * Get trashed innovators
     *
     * @return App\Innovator::trashed
     */
    public function getTrashedInnovators()
    {
        return Innovator::onlyTrashed()->get([
            'company_name',
            'company_bio',
            'product_information',
            'country',
            'company_email',
            'website_url',
            'slug',
            'id',
            'verified'
        ]);
    }

    /**
     * force delete an innovator
     *
     * @return Innovator::delete
     */
    public function forceDeleteInnovator($innovator)
    {
        $innovator = Innovator::onlyTrashed()->where('slug', $innovator)->first();
        $innovator->forceDelete();

        return "The innovator was deleted";
    }


    /**
     * Restore an innovator
     *
     */
    public function restoreInnovator($innovator)
    {
        $innovator = Innovator::onlyTrashed()->where('slug', $innovator)->first();
        $innovator->restore();
    }


    /**
     * Update innovator's logo
     *
     */
    public function updateInnovatorLogo(Innovator $innovator, Request $request)
    {
        $company_name = $innovator->company_name;
        $path = $request->logo->storeAs('innovators', str_slug($company_name, '-') . '.' . $request->file('logo')->extension(), 'public');

        $innovator->logo = $path;
        $innovator->save();

        return response($company_name . '\'s Logo has been updated');
    }

    /**
     * Contact a user
     *
     */
    public function contactUser(Request $request)
    {
        $subject = $request->subject;
        $mess = $request->message;
        $email = $request->email;

        $message = collect(['subject' => $subject, 'mess' => $mess, 'email' => $email]);
        Mail::to($email)->send(new ContactUser($message));

        return "mail sent";
    }
}
