<?php

namespace App\Http\Middleware;

use Closure;

class UpdateActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->user()->last_activity = now();
        return $next($request);
    }
}
