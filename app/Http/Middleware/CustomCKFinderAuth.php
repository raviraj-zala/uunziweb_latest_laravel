<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CustomCKFinderAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        config(['ckfinder.authentication' => function () {
            return  $this->authorize();
        }]);
        return $next($request);
    }


    /**
     *  allow
     */
    public function authorize()
    {
        return env('CKFINDER');
    }
}
