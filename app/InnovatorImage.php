<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InnovatorImage extends Model
{
    public $table = 'innovator_image';
    public $timestamps = false;
}
