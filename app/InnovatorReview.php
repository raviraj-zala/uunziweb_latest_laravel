<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InnovatorReview extends Model
{
    //mass assignable
    protected $fillable = [
        "user_id",
        "innovator_id",
        "comment",
        "rating"
    ];

    /**
     * This belongs to a post
     */
    public function innovator()
    {
        $this->hasOne('App\Innovator');
    }

    /**
     * A review belongs to a user
     * 
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Lazy load users
     */
    protected $with = ['user'];
}
