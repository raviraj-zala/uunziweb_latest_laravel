<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    //mass assignable attributes
    protected $fillable = [
        'image',
        'title',
        'slug',
        'author',
        'description',
        'body',
        'published_at'
    ];

    protected $with = 'tags';

    /**
     * A post has many categories
     * a category has many category
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    /**
     * Route key name
     * 
     */
    public function getRouteKeyName()
    {
        return "slug";
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published_at' => 'datetime:Y-m-d',
    ];

    protected $hidden = ['pivot', 'created_at', 'updated_at', 'deleted_at'];
}
