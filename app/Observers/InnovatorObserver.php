<?php

namespace App\Observers;

use App\Innovator;
use App\Notifications\ListingDeleted;

class InnovatorObserver
{
    /**
     * Handle the innovator "created" event.
     *
     * @param  \App\Innovator  $innovator
     * @return void
     */
    public function created(Innovator $innovator)
    {
        //
    }

    /**
     * Handle the innovator "updated" event.
     *
     * @param  \App\Innovator  $innovator
     * @return void
     */
    public function updated(Innovator $innovator)
    {
        //
    }

    /**
     * Handle the innovator "deleted" event.
     *
     * @param  \App\Innovator  $innovator
     * @return void
     */
    public function deleted(Innovator $innovator)
    {
        //
    }

    /**
     * Handle the innovator "restored" event.
     *
     * @param  \App\Innovator  $innovator
     * @return void
     */
    public function restored(Innovator $innovator)
    {
        //
    }

    /**
     * Handle the innovator "force deleted" event.
     *
     * @param  \App\Innovator  $innovator
     * @return void
     */
    public function forceDeleted(Innovator $innovator)
    {
       return $innovator->notify(new ListingDeleted());
    }
}
