<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InnovatorCategory extends Model
{
    //mass assignable attributes
    protected $fillable = [
        'name'
    ];

    /**
     * A category has many innovators
     * 
     */
    public function innovators()
    {
        return $this->hasMany('App\Innovator');
    }
}
