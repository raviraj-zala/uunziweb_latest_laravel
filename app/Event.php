<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //mass assignable atrributes
    protected $fillable = [
        'date',
        'title',
        'starting_time',
        'ending_time',
        'duration',
        'venue',
        'information',
        'external_links',
    ];
}
