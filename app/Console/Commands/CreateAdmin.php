<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command to add an admin to the application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Are you sure you want to add an admin to the application ?')) {
            $this->line("Ok fill in the correct details below");
            $name = $this->ask("Full name", "John Doe");
            $email = $this->ask('Email address', 'john@example.com');
            $password = $this->secret('Password');
            $password_confirmation = $this->secret('Confirm Passowrd');

            if ($password == $password_confirmation) {
                if (strlen($password_confirmation) <= 7) {
                    return $this->error('Your password has to be more than 8 characters Please run the command again');
                }
                if (count(User::where('email', $email)->get()) > 0) {
                    return $this->error('User Already exists');
                }

                $user = new User;
                $user->name = $name;
                $user->email = $email;
                $user->password = Hash::make($password_confirmation);
                $user->role = "admin";
                $user->save();
                return $this->line($name . ' has been added to ' . config('app.name') . ' as an adminstrator');
            }

            return $this->error('The given passwords don\'t match please run the command again');
        }

        return $this->line('You have cancelled this command');
    }
}
