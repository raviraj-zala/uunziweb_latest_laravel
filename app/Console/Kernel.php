<?php

namespace App\Console;

use App\GooglePlayReview;
use App\Innovator;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Raulr\GooglePlayScraper\Scraper;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

//        $schedule->call(function () {
//
//            $users = User::where('email_verified_at', null)
//                ->where('created_at', '>', Carbon::now()->subDays(90))
//                ->get();
//            foreach ($users as $user) {
//                User::where('id', $user->id)->delete();
//            }
//        })->daily();

        $schedule->call(function () {

            GooglePlayReview::query()->truncate();
            ini_set('max_execution_time', 30000);
            $innovators = Innovator::all();
            foreach ($innovators as $key => $innovator) {
                if ($innovator->website_url) {
                    $res = explode('=', $innovator->website_url);
                    if (count($res) > 1) {
                        if ($res[1] != null) {
                            $ress = $res[1];
                            $ress = explode('&', $ress);

                            $pkgname = $ress[0];

                            $scraper = new Scraper();

                            $app = $scraper->getApp($pkgname);
                            if ($app != "fail") {
                                $addreview = new GooglePlayReview();
                                $addreview->innovator_id = $innovator->id;
                                $addreview->rating = $app['rating'];
                                $addreview->votes = $app['votes'];
                                $addreview->save();
                            }


                        }

                    }
                }
            }
        })->daily();


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
