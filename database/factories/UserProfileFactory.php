<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\UserProfile;
use Faker\Generator as Faker;

$factory->define(UserProfile::class, function (Faker $faker) {
    return [
        'user_bio' => $faker->text,
        'phone' => $faker->phoneNumber,
        'date_of_birth' => $faker->date(),
        'occupation' => $faker->jobTitle
    ];
});
