<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\InnovatorReview;
use Faker\Generator as Faker;

$factory->define(InnovatorReview::class, function (Faker $faker) {
    return [
        "innovator_id" => $faker->unique(true, 1000000)->numberBetween(1, 50),
        "user_id" => $faker->unique(true, 1000000)->numberBetween(1, 100),
        "comment" => $faker->text(),
        "rating" => $faker->numberBetween(1,5)
    ];
});
