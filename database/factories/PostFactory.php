<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $title = $faker->text(50);
    $slug = str_slug($title);
    return [
        'image' => $faker->imageUrl(640,480,null,true,null,false),
        'title' => $title,
        'slug' => $slug,
        'author' => $faker->name(),
        'description' => $faker->text(150),
        'body' => $faker->text(790),
        'published_at' => $faker->date()
    ];
});
