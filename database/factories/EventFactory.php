<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {

    $starts = $faker->time();
    $ends = $faker->time();
    $duration = strtotime($starts) - strtotime($ends);
    return [
        'date' => $faker->dateTimeThisYear,
        'title' => $faker->text(50),
        'starting_time' => $starts,
        'ending_time' => $ends,
        'duration' =>  date('H:i' ,$duration),
        'venue' => $faker->address,
        'information' => $faker->text(),
        'external_links' => $faker->url
    ];
});
