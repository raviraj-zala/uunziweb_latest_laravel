<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\InnovatorCategory;
use Faker\Generator as Faker;

$factory->define(InnovatorCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
