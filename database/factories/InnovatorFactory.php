<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Innovator;
use Faker\Generator as Faker;

$factory->define(Innovator::class, function (Faker $faker) {
    $company = $faker->unique()->company;
    return [
        'company_name' => $company,
        'slug' => str_slug($company),
        'company_tagline' => $faker->text(25),
        'company_bio' => $faker->text(100),
        'product_information' => $faker->paragraph,
        'additional_information' => $faker->paragraph,
        'country' => $faker->country,
        'cost' => $faker->text,
        'benefits' => $faker->text(),
        'sign_up_information' => $faker->url,
        'contact_information' => $faker->phoneNumber,
        'company_email' => $faker->companyEmail,
        'website_url' => $faker->url,
        'logo' => $faker->imageUrl(),
        'user_id' => null,
        'verified' => $faker->boolean
    ];
});
