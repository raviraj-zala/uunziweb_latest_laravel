<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserProfile;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 100)->create()->each(function ($user) {
            $user->profile()->save(factory(UserProfile::class)->make());
        });
    }
}
