<?php

use Illuminate\Database\Seeder;
use App\Innovator;
use App\InnovatorCategory;
use App\InnovatorReview;

class InnovatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Innovator::class, 100)->create()->each(function($innovator){
            $innovator->categories()->save(factory(InnovatorCategory::class)->make());
           // $innovator->reviews()->save(factory(InnovatorReview::class)->make());
        });
    }
}
