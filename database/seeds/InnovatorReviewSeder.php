<?php

use Illuminate\Database\Seeder;
use App\InnovatorReview;

class InnovatorReviewSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(InnovatorReview::class, 100)->create();
    }
}
