<?php

use Illuminate\Database\Seeder;
use App\InnovatorCategory;

class InnovatorCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(InnovatorCategory::class, 1)->create();
    }
}
