<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInnovatorInnovatorCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('innovator_innovator_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('innovator_id')->default(null);
            $table->unsignedBigInteger('innovator_subcategory_id')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('innovator_innovator_category');
    }
}
