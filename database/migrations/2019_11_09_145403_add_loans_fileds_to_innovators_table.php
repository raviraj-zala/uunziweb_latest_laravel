<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoansFiledsToInnovatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('innovators', function (Blueprint $table) {

            $table->unsignedBigInteger('minimum_loan_value')->nullable();
            $table->unsignedBigInteger('maximum_loan_value')->nullable();
            $table->unsignedBigInteger('repayment_period_min')->nullable();
            $table->unsignedBigInteger('repayment_period_max')->nullable();
            $table->unsignedDecimal('interest_rate_min', 10, 1)->nullable();
            $table->unsignedDecimal('interest_rate_max', 10, 1)->nullable();
            $table->text('interest_rate_mode')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('innovators', function (Blueprint $table) {
            //
        });
    }
}
