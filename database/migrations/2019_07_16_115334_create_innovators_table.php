<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInnovatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('innovators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name')->unique();
            $table->string('slug')->unique();

            $table->text('company_tagline')->nullable();

            $table->text('company_bio')->nullable();
            $table->longText('product_information')->nullable();
            $table->longText('additional_information')->nullable();
            $table->string('country')->nullable();
            $table->text('cost')->nullable();
            $table->text('benefits')->nullable();
            $table->text('sign_up_information')->nullable();
            $table->text('contact_information')->nullable();
            $table->string('company_email')->nullable();
            $table->string('website_url')->nullable();
            $table->string('logo')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->boolean('verified')->nullable();

            $table->unsignedBigInteger('innovator_subcategory_id')->nullable();



            $table->unsignedBigInteger('innovator_category_id')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('innovators');
    }
}
